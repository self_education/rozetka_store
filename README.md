# AlloStore

## Description

This project provides end-to-end UI testing for the Allo Online Store in Ukraine (https://allo.ua/).

The project implements the Page Object, Page Factory, and Fluent Interface patterns, adhering to SOLID principles. This is a non-commercial project created independently to expand my knowledge in test automation and environment creation.

Future plans include adding API tests, which may be part of a separate project. If that happens, I will provide a link here.

Development of the test environment continues in my free time.


## Installation Instructions

1. Clone the repository:

        git clone https://gitlab.com/self_education/allo_store.git

2. Navigate to the project directory:

        cd AlloStore

3. Ensure you have Maven installed. If not, download and install Maven from here.(https://maven.apache.org/install.html)

4. Install the project dependencies:

## Test Configuration

To run the test suite, ensure the following parameters are included in your TestNG XML suite configuration:

        <suite name="AlloStore Test Suite">
            <parameter name="baseURL" value="https://allo.ua/"/>
            <parameter name="chromeOptions" value="--remote-allow-origins=*"/>
            <parameter name="dimensionWidth" value="1920"/>
            <parameter name="dimensionHeight" value="1080"/>
            <test name="All Tests">
                <classes>
                    <class name="org.example.BeforeRun"/>
                </classes>
            </test>
        </suite>

Please note that the test suite configuration is private and not shared.

## Contact Information

For any questions or suggestions, please contact me at kab.alex.it@example.com.
