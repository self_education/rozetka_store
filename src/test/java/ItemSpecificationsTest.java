import org.example.HeaderActions;
import org.testng.annotations.Test;

public class ItemSpecificationsTest {
    @Test
    public void ClickCatalogMenu() {
        String itemName = "xiomi readmi 10 pro";
        String itemName1 = "Телевізор Xiaomi TV A2 32";
        String itemName2 = "Телевізор Xiaomi TV A2 32 (У1)";
        String itemName3 = "Телевізор Xiaomi TV A Pro 43";
        String itemPrice = "13\u00A0999";
        String code = "959124";
        String code1 = "971680";
        String code2 = "1017690";

        HeaderActions action = new HeaderActions();
        action
                .searchItem(itemName)
                .clickUnExistedItemInSearch(itemName)
                .verifyCatalogEmpty()
                .verifyItemsFoundEquality(0, itemName)

                .searchItem(itemName1)
                .clickAllResults()
                .verifyItemsFoundEquality(5, itemName1)

                .searchItem(itemName1)
                .clickItemByNameInSearch(itemName1)
                .verifyItemName(itemName1)
                .verifyTVType("LED")
                .verifyItemCode(code)
                .verifyItemInStock()

                .searchItem(itemName1)
                .clickItemByNameInSearch(itemName2)
                .verifyItemName(itemName2)
                .verifyTVType("LED")
                .verifyItemCode(code1)
                .verifyItemOutOfStock()

                .searchItem(itemName3)
                .clickItemByNameAndPriceInSearch(itemName3, itemPrice)
                .verifyItemName(itemName3)
                .verifyTVType("LED")
                .verifyItemCode(code2)
                .verifyItemInAlloStores();
    }
}
