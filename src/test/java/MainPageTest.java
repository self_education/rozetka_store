import org.example.HeaderActions;
import org.testng.annotations.Test;

public class MainPageTest {

    @Test
    public void ClickCatalogMenu(){
        HeaderActions action = new HeaderActions();
        action
                .clickCatalogMenu()
                .clickTopSales()
                .clickPromoDetails();
    }
}
