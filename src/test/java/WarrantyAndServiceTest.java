import org.example.CommonConstants;
import org.example.HeaderActions;
import org.example.WarrantyAndServiceDataBase;
import org.testng.annotations.Test;

import static org.example.WarrantyAndServiceDataBase.RETURN_CHECK_REPAIR_REQUEST_NOT_FOUND_POP_UP_CODE;

public class WarrantyAndServiceTest {

    @Test
    public void ClickCatalogMenu() throws InterruptedException {
        HeaderActions action = new HeaderActions();
        action
                .clickWarrantyAndService()
                .verifyWarrantyOnItemsHeaderVisible()
                .verifyWarrantyPrepareItemHeader()
                .verifyWarrantyPrepareDocsHeader()
                .verifyWarrantySendItemToServiceHeader()
                .verifyWarrantyHowToSendItemToServiceHeader()

                .clickExchangeAndReturnButton()
                .verifyExchangeAndReturnHeader()
                .verifyReturnUnsatisfactoryProductHeader()
                .verifyReturnPrepareDocsHeader()
                .verifyReturnIfNOTOutOfTimeHeaderHeader()

                .clickServiceCentreActButton()
                .verifyServiceCentreActHeader()

                .clickServiceCentresButton()
                .verifyServiceCentresAddressesHeader()

                .clickCheckRepairStatusButton()
                .verifyCheckRepairRequestStatusHeader()

                .clickCheckRepairRequestStatusButton()
                .verifyCheckRepairRequestStatusPopUpHeader()
                .verifyCheckRepairRequestStatusPopUpDescription()

                .clickCheckTheStatusButton()
                .verifyCheckRepairRequestValidationTextExists()

                .inputCheckRepairRequestStatusPopUpID(CommonConstants.ONE.getDigit())
                .clickCheckTheStatusButton()
                .verifyCheckRepairRequestNumbersOnlyTextExists()

                .inputCheckRepairRequestStatusPopUpID(RETURN_CHECK_REPAIR_REQUEST_NOT_FOUND_POP_UP_CODE.getMessage())
                .clickCheckTheStatusButton()
                .verifyCheckRepairRequestNotFoundPopUpTextExists()
                .verifyCheckRepairRequestContactInfoMessageExists()
                .clickCheckRepairRequestCloseButton()

                .clickTheFAQsButton()
                .clickTheFAQsTotFitButton()
                .verifyTheFAQsNotFitButtonTextCorrect()
                .verifyTheFAQsCannotBeReturned()
                .verifyTheFAQsCheckLost()
                .verifyTheFAQsWarrantyPeriod()
                .verifyTheFAQsBreaksDownDuringTheWarrantyPeriod()
                .verifyTheFAQsVerifyXiaomiIMEI()

                .clickReviewsButton()
                .verifyReviewsHeaderExists()
                .clickFormReviewsNumberNineRadioButton(CommonConstants.ONE.getInt(), CommonConstants.ONE.getInt())
                .clickFormReviewsNumberNineRadioButton(CommonConstants.TWO.getInt(), CommonConstants.TWO.getInt())
                .clickFormReviewsNumberNineRadioButton(CommonConstants.THREE.getInt(), CommonConstants.THREE.getInt())
                .clickFormReviewsNumberNineRadioButton(CommonConstants.FOUR.getInt(), CommonConstants.FOUR.getInt())
                .verifyReviewsQuestionOneHeader()
                .verifyReviewsQuestionTwoHeader()
                .verifyReviewsQuestionThreeHeader()
                .verifyReviewsQuestionFourHeader()
                .verifyReviewsQuestionLastStepHeaderExists()
                .verifyFormReviewsNumberBackgroundColorIsCorrect(CommonConstants.ONE.getInt(), CommonConstants.ONE.getInt(),
                        WarrantyAndServiceDataBase.NUMBER_BACKGROUND_IF_ACTIVE_COLOR.getMessage())
                .verifyFormReviewsNumberBackgroundColorIsCorrect(CommonConstants.ONE.getInt(), CommonConstants.TWO.getInt(),
                        WarrantyAndServiceDataBase.NUMBER_BACKGROUND_IF_NOT_ACTIVE_COLOR.getMessage());
    }
}
