import org.example.HeaderActions;
import org.testng.annotations.Test;

public class ShipmentPaymentTest {

    @Test
    public void ClickCatalogMenu() {
        HeaderActions action = new HeaderActions();
        action
                .clickShipmentPayment()
                .verifyHowToPlaceAnOrderHeaderVisible()
                .verifyHowToPlaceAnOrderInfoVisible()
                .verifyCallCentreNumberCorrect()
                .verifyCallCentreWorkingHoursCorrect()

                .clickHowGetDButton()
                .verifyHowTOGetProductVisible()
                .verifyPickupFromTheStoreVisible()
                .verifyDeliveryToTheApartmentVisible()
                .verifyPickupFromNovaPoshtaVisible()
                .verifyPickupFromMeestPostVisible()
                .verifyDeliveryByMeestPostVisible()
                .verifyDeliveryByNovaPoshtaVisible()

                .clickWhenButton()
                .verifyNovaPoshtaImgVisible()
                .verifyMeestImgVisible()

                .clickPriceButton()
                .verifyDeliveryInfoVisible()

                .clickPaymentButton()
                .verifyCashlessPaymentVisible()
                .verifyCashPaymentVisible()
                .verifyCardPaymentVisible()

                .clickCheckButton()
                .verifyRecipientsRightsVisible()
                .verifyExaminationPhoneVisible()
                .verifyExaminationLaptopsVisible()
                .verifyExaminationTVsVisible()
                .verifyExaminationPhotoVideoEquipmentVisible()
                .verifyExaminationHVACVisible()
                .verifyExaminationSmallAppliancesVisible()
                .verifyAdditionalQuestionsParagraphVisible();
    }
}
