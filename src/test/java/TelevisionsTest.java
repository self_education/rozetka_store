import org.example.HeaderActions;
import org.example.TelevisionsActions;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TelevisionsTest {

    @Test
    @Parameters({"productName", "productName1"})
    public void ClickCatalogMenu(String productName, String productName1) {
        HeaderActions action = new HeaderActions();
        action
                .clickCatalogMenu()
                .clickTelevisionsAndMultimedia()
                .clickTelevisions()
                .verifyProductExists(productName, TelevisionsActions.class)
                .verifyProductNotExists(productName1, TelevisionsActions.class)
                .clickLEDTelevisionType()
                .clickShowButton()
                .verifyProductExists(productName, TelevisionsActions.class)
                .verifyProductNotExists(productName1, TelevisionsActions.class)
                .clickProductByName(productName);
    }
}
