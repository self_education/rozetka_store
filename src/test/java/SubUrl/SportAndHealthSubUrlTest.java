package SubUrl;

import org.example.HeaderActions;
import org.example.UrlDataBase;
import org.testng.annotations.Test;

public class SportAndHealthSubUrlTest {

    @Test
    public void ClickCatalogMenu() {
        HeaderActions action = new HeaderActions();
        action
                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickBicyclesAndAccessories()
                .verifyHeaderUrl(UrlDataBase.BICYCLES_AND_ACCESSORIES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickSkateboards()
                .verifyHeaderUrl(UrlDataBase.SKATEBOARDS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickDecksForSkateboards()
                .verifyHeaderUrl(UrlDataBase.DECKS_FOR_SKATEBOARDS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickWheelsForSkateboards()
                .verifyHeaderUrl(UrlDataBase.WHEELS_FOR_SKATEBOARDS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickAccessoriesForSkateboards()
                .verifyHeaderUrl(UrlDataBase.SPARE_PARTS_FOR_SKATEBOARDS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickSlimmingClothes()
                .verifyHeaderUrl(UrlDataBase.SLIMMING_CLOTHES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickSimulators()
                .verifyHeaderUrl(UrlDataBase.SIMULATORS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickSportsEquipment()
                .verifyHeaderUrl(UrlDataBase.SPORTS_EQUIPMENT_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickMartialArts()
                .verifyHeaderUrl(UrlDataBase.MARTIAL_ARTS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickDarts()
                .verifyHeaderUrl(UrlDataBase.DARTS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickTennisAndSquash()
                .verifyHeaderUrl(UrlDataBase.TENNIS_AND_SQUASH_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickRollersAndProtection()
                .verifyHeaderUrl(UrlDataBase.ROLLERS_AND_PROTECTION_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickWinterSports()
                .verifyHeaderUrl(UrlDataBase.WINTER_SPORTS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickWaterSports()
                .verifyHeaderUrl(UrlDataBase.WATER_SPORTS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickHealthAndCare()
                .verifyHeaderUrl(UrlDataBase.HEALTH_AND_CARE_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickHearingAids()
                .verifyHeaderUrl(UrlDataBase.HEARING_AIDS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickCardiacSensors()
                .verifyHeaderUrl(UrlDataBase.CARDIAC_SENSORS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickWalkieTalkies()
                .verifyHeaderUrl(UrlDataBase.WALKIE_TALKIES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickAccessoriesForWalkieTalkies()
                .verifyHeaderUrl(UrlDataBase.ACCESSORIES_FOR_WALKIE_TALKIES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickBasketball()
                .verifyHeaderUrl(UrlDataBase.BASKETBALL_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickBilliards()
                .verifyHeaderUrl(UrlDataBase.BILLIARDS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickMiniGolf()
                .verifyHeaderUrl(UrlDataBase.MINI_GOLF_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickProtectiveEquipment()
                .verifyHeaderUrl(UrlDataBase.PROTECTIVE_EQUIPMENT_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickTrampolinesAndAccessories()
                .verifyHeaderUrl(UrlDataBase.TRAMPOLINES_AND_ACCESSORIES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickGymnastics()
                .verifyHeaderUrl(UrlDataBase.GYMNASTICS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickHockey()
                .verifyHeaderUrl(UrlDataBase.HOCKEY_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickFigureSkating()
                .verifyHeaderUrl(UrlDataBase.FIGURE_SKATING_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickSportsCups()
                .verifyHeaderUrl(UrlDataBase.SPORTS_CUPS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickSportAndHealth()
                .clickSportsMedals()
                .verifyHeaderUrl(UrlDataBase.SPORTS_MEDALS_PAGE.getUrl());

    }
}
