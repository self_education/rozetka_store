package SubUrl;

import org.example.HeaderActions;
import org.example.UrlDataBase;
import org.testng.annotations.Test;

public class PhonesSubUrlTest {

    @Test
    public void ClickCatalogMenu() {
        HeaderActions action = new HeaderActions();
        action
                .clickCatalogMenu()
                .clickPhones()
                .clickSmartphones()
                .verifyHeaderUrl(UrlDataBase.SMARTPHONE_PAGE.getUrl())

                .clickCatalogMenu()
                .clickPhones()
                .clickMobile()
                .verifyHeaderUrl(UrlDataBase.PHONES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickPhones()
                .clickPhoneForHome()
                .verifyHeaderUrl(UrlDataBase.PHONE_FOR_HOME_PAGE.getUrl())

                .clickCatalogMenu()
                .clickPhones()
                .clickPhoneAccessories()
                .verifyHeaderUrl(UrlDataBase.PHONE_ACCESSORIES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickPhones()
                .clickPhoneSpareParts()
                .verifyHeaderUrl(UrlDataBase.PHONE_SPARE_PARTS_PAGE.getUrl());
    }
}
