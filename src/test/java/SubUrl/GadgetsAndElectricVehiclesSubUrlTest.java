package SubUrl;

import org.example.HeaderActions;
import org.example.UrlDataBase;
import org.testng.annotations.Test;

public class GadgetsAndElectricVehiclesSubUrlTest {

    @Test
    public void ClickCatalogMenu() {
        HeaderActions action = new HeaderActions();
        action
                .clickCatalogMenu()
                .clickGadgetsAndElectricVehicles()
                .click3D_Print()
                .verifyHeaderUrl(UrlDataBase.PRINT_3D_PAGE.getUrl())

                .clickCatalogMenu()
                .clickGadgetsAndElectricVehicles()
                .clickFitnessBracelets()
                .verifyHeaderUrl(UrlDataBase.FITNESS_BRACELETS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickGadgetsAndElectricVehicles()
                .clickAccessoriesForSmartwatches()
                .verifyHeaderUrl(UrlDataBase.ACCESSORIES_FOR_SMARTWATCHES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickGadgetsAndElectricVehicles()
                .clickElectricTransport()
                .verifyHeaderUrl(UrlDataBase.ELECTRIC_TRANSPORT_PAGE.getUrl())

                .clickCatalogMenu()
                .clickGadgetsAndElectricVehicles()
                .clickGameZone()
                .verifyHeaderUrl(UrlDataBase.GAME_ZONE_PAGE.getUrl())

                .clickCatalogMenu()
                .clickGadgetsAndElectricVehicles()
                .clickActionCamerasAndAccessories()
                .verifyHeaderUrl(UrlDataBase.ACTION_CAMERAS_AND_ACCESSORIES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickGadgetsAndElectricVehicles()
                .clickTobaccoHeatingSystems()
                .verifyHeaderUrl(UrlDataBase.TOBACCO_HEATING_SYSTEMS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickGadgetsAndElectricVehicles()
                .clickSmartKeyrings()
                .verifyHeaderUrl(UrlDataBase.SMART_KEY_RINGS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickGadgetsAndElectricVehicles()
                .clickSmartRings()
                .verifyHeaderUrl(UrlDataBase.SMART_RINGS_PAGE.getUrl());
    }
}
