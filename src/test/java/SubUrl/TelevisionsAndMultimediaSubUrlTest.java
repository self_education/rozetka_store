package SubUrl;

import org.example.HeaderActions;
import org.example.UrlDataBase;
import org.testng.annotations.Test;

public class TelevisionsAndMultimediaSubUrlTest {

    @Test
    public void ClickCatalogMenu() {
        HeaderActions action = new HeaderActions();
        action
                .clickCatalogMenu()
                .clickTelevisionsAndMultimedia()
                .clickTelevisions()
                .verifyHeaderUrl(UrlDataBase.TELEVISIONS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickTelevisionsAndMultimedia()
                .clickDigitalPhotoFrames()
                .verifyHeaderUrl(UrlDataBase.DIGITAL_PHOTO_FRAMES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickTelevisionsAndMultimedia()
                .clickProjectionEquipment()
                .verifyHeaderUrl(UrlDataBase.PROJECTION_EQUIPMENT_PAGE.getUrl())

                .clickCatalogMenu()
                .clickTelevisionsAndMultimedia()
                .clickPortableTVs()
                .verifyHeaderUrl(UrlDataBase.PORTABLE_TVS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickTelevisionsAndMultimedia()
                .clickPhotoAndVideoEquipment()
                .verifyHeaderUrl(UrlDataBase.PHOTO_AND_VIDEO_EQUIPMENT_PAGE.getUrl())

                .clickCatalogMenu()
                .clickTelevisionsAndMultimedia()
                .clickPlayersMP3()
                .verifyHeaderUrl(UrlDataBase.PLAYERS_MP3_PAGE.getUrl());

    }
}
