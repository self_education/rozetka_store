package SubUrl;

import org.example.HeaderActions;
import org.example.UrlDataBase;
import org.testng.annotations.Test;

public class AudioSubUrlTest {

    @Test
    public void ClickCatalogMenu() {
        HeaderActions action = new HeaderActions();
        action
                .clickCatalogMenu()
                .clickAudio()
                .clickHeadphones()
                .verifyHeaderUrl(UrlDataBase.HEADPHONES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickAudio()
                .clickWirelessHeadphones()
                .verifyHeaderUrl(UrlDataBase.WIRELESS_HEADPHONES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickAudio()
                .clickWiredHeadphones()
                .verifyHeaderUrl(UrlDataBase.WIRED_HEADPHONES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickAudio()
                .clickPortableSpeakers()
                .verifyHeaderUrl(UrlDataBase.PORTABLE_SPEAKERS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickAudio()
                .clickSpeakersForPCs()
                .verifyHeaderUrl(UrlDataBase.SPEAKERS_FOR_PCS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickAudio()
                .clickHeadphoneAccessories()
                .verifyHeaderUrl(UrlDataBase.HEADPHONE_ACCESSORIES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickAudio()
                .clickAcousticSystems()
                .verifyHeaderUrl(UrlDataBase.ACOUSTIC_SYSTEMS_PAGE.getUrl());

    }
}
