package SubUrl;

import org.example.HeaderActions;
import org.example.UrlDataBase;
import org.testng.annotations.Test;

public class RepairAndEquipmentSubUrlTest {

    @Test
    public void ClickCatalogMenu() {
        HeaderActions action = new HeaderActions();
        action
                .clickCatalogMenu()
                .clickRepairAndEquipment()
                .clickPlumbing()
                .verifyHeaderUrl(UrlDataBase.PLUMBING_PAGE.getUrl())

                .clickCatalogMenu()
                .clickRepairAndEquipment()
                .clickRepair()
                .verifyHeaderUrl(UrlDataBase.REPAIR_PAGE.getUrl())

                .clickCatalogMenu()
                .clickRepairAndEquipment()
                .clickElectricalInstallationEquipment()
                .verifyHeaderUrl(UrlDataBase.ELECTRICAL_INSTALLATION_EQUIPMENT_PAGE.getUrl());

    }
}
