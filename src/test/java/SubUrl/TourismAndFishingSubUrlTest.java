package SubUrl;

import org.example.HeaderActions;
import org.example.UrlDataBase;
import org.testng.annotations.Test;

public class TourismAndFishingSubUrlTest {

    @Test
    public void ClickCatalogMenu() {
        HeaderActions action = new HeaderActions();
        action
                .clickCatalogMenu()
                .clickTourismAndFishing()
                .clickTents()
                .verifyHeaderUrl(UrlDataBase.TENTS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickTourismAndFishing()
                .clickLanterns()
                .verifyHeaderUrl(UrlDataBase.LANTERNS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickTourismAndFishing()
                .clickThermalProducts()
                .verifyHeaderUrl(UrlDataBase.THERMAL_PRODUCTS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickTourismAndFishing()
                .clickBackpacks()
                .verifyHeaderUrl(UrlDataBase.BACKPACKS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickTourismAndFishing()
                .clickInflatableBoats()
                .verifyHeaderUrl(UrlDataBase.BOATS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickTourismAndFishing()
                .clickMotorsForBoats()
                .verifyHeaderUrl(UrlDataBase.MOTORS_FOR_BOATS_PAGE.getUrl());

    }
}
