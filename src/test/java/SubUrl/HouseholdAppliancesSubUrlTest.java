package SubUrl;

import org.example.HeaderActions;
import org.example.UrlDataBase;
import org.testng.annotations.Test;

public class HouseholdAppliancesSubUrlTest {

    @Test
    public void ClickCatalogMenu() {
        HeaderActions action = new HeaderActions();
        action
                .clickCatalogMenu()
                .clickHouseholdAppliances()
                .clickClimateControlEquipment()
                .verifyHeaderUrl(UrlDataBase.CLIMATE_CONTROL_EQUIPMENT_PAGE.getUrl())

                .clickCatalogMenu()
                .clickHouseholdAppliances()
                .clickHomeAppliances()
                .verifyHeaderUrl(UrlDataBase.HOME_APPLIANCES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickHouseholdAppliances()
                .clickCoffeeWorld()
                .verifyHeaderUrl(UrlDataBase.COFFEE_WORLD_PAGE.getUrl())

                .clickCatalogMenu()
                .clickHouseholdAppliances()
                .clickKitchenAppliances()
                .verifyHeaderUrl(UrlDataBase.KITCHEN_APPLIANCES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickHouseholdAppliances()
                .clickPersonalisedCare()
                .verifyHeaderUrl(UrlDataBase.PERSONALISED_CARE_PAGE.getUrl())

                .clickCatalogMenu()
                .clickHouseholdAppliances()
                .clickLargeHouseholdAppliances()
                .verifyHeaderUrl(UrlDataBase.LARGE_HOUSEHOLD_APPLIANCES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickHouseholdAppliances()
                .clickBuilt_inAppliances()
                .verifyHeaderUrl(UrlDataBase.BUILT_IN_APPLIANCES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickHouseholdAppliances()
                .clickAccessories()
                .verifyHeaderUrl(UrlDataBase.ACCESSORIES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickHouseholdAppliances()
                .clickWaterFilters()
                .verifyHeaderUrl(UrlDataBase.WATER_FILTERS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickHouseholdAppliances()
                .clickSparePartsForHouseholdAppliances()
                .verifyHeaderUrl(UrlDataBase.SPARE_PARTS_FOR_HOUSEHOLD_APPLIANCES_PAGE.getUrl());

    }
}
