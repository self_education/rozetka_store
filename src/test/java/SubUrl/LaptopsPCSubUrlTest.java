package SubUrl;

import org.example.HeaderActions;
import org.example.UrlDataBase;
import org.testng.annotations.Test;

public class LaptopsPCSubUrlTest {

    @Test
    public void ClickCatalogMenu() {
        HeaderActions action = new HeaderActions();
        action
                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickTablets()
                .verifyHeaderUrl(UrlDataBase.TABLETS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickLaptops()
                .verifyHeaderUrl(UrlDataBase.LAPTOPS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickNetworkEquipment()
                .verifyHeaderUrl(UrlDataBase.NETWORK_EQUIPMENT_PAGE.getUrl())

                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickMonitors()
                .verifyHeaderUrl(UrlDataBase.MONITORS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickPersonalComputers()
                .verifyHeaderUrl(UrlDataBase.PERSONAL_COMPUTERS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickMonoBlocks()
                .verifyHeaderUrl(UrlDataBase.MONO_BLOCKS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickNetTops()
                .verifyHeaderUrl(UrlDataBase.NET_TOPS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickAccessoriesForTablets()
                .verifyHeaderUrl(UrlDataBase.ACCESSORIES_FOR_TABLETS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickAccessoriesForLaptops()
                .verifyHeaderUrl(UrlDataBase.ACCESSORIES_FOR_LAPTOPS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickPeripherals()
                .verifyHeaderUrl(UrlDataBase.PERIPHERALS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickE_Books()
                .verifyHeaderUrl(UrlDataBase.E_BOOKS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickGraphicTablets()
                .verifyHeaderUrl(UrlDataBase.GRAPHIC_TABLETS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickAccessoriesForGraphicTablets()
                .verifyHeaderUrl(UrlDataBase.ACCESSORIES_FOR_GRAPHIC_TABLETS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickLaptopParts()
                .verifyHeaderUrl(UrlDataBase.LAPTOP_PARTS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickPersonalComputerParts()
                .verifyHeaderUrl(UrlDataBase.PERSONAL_COMPUTER_PARTS_PAGE.getUrl())

                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickAccessoriesForPCCases()
                .verifyHeaderUrl(UrlDataBase.ACCESSORIES_FOR_PC_CASES_PAGE.getUrl())

                .clickCatalogMenu()
                .clickLaptopsPC()
                .clickServers()
                .verifyHeaderUrl(UrlDataBase.SERVERS_PAGE.getUrl());

    }
}
