import org.example.HeaderActions;
import org.testng.annotations.Test;

public class ApplePageTest {

    @Test
    public void ClickCatalogMenu() {
        HeaderActions action = new HeaderActions();
        action
                .clickCatalogMenu()
                .clickApple()

                .clickMac()
                .verifyMacDescriptionInVisibleArea()

                .clickIPhone()
                .verifyIPhoneDescriptionInVisibleArea()

                .clickIPad()
                .verifyIPadDescriptionInVisibleArea()

                .clickWatch()
                .verifyWatchDescriptionInVisibleArea()

                .clickAirTag()
                .verifyAirTagDescriptionInVisibleArea()

                .clickTV()
                .verifyTVDescriptionInVisibleArea()

                .clickMusic()
                .verifyMusicDescriptionInVisibleArea()

                .verifyFooterIsDisplayed()
                .verifyIPhoneDescriptionIsDisplayed();
    }
}
