import org.example.HeaderActions;
import org.testng.annotations.Test;

public class TradeInTest {

    @Test
    public void ClickCatalogMenu() {
        HeaderActions action = new HeaderActions();
        action
                .clickTradeIn()
                .verifyBreadcrumbsTradeInIsDisplayed()
                .verifyThreeStepsForTradeInIsDisplayed()
                .verifyFirstStepHeaderIsDisplayed()

                .verifyPhoneNewDeviceTabActive()
                .verifyTabletNewDeviceTabInactive()
                .verifyGadgetNewDeviceTabInactive()
                .verifyLaptopNewDeviceTabInactive()

                .verifySmartphoneOldDeviceTabActive()
                .verifyTabletOldDeviceTabInactive()
                .verifyGadgetOldDeviceTabInactive()
                .verifyCameraOldDeviceTabInactive()
                .verifyTVOldDeviceTabInactive()
                .verifyLaptopOldDeviceTabInactive()
                .verifyConsoleOldDeviceTabInactive()

                .clickGadgetNewDeviceTab()
                .verifyPhoneNewDeviceTabInactive()
                .verifyTabletNewDeviceTabInactive()
                .verifyGadgetNewDeviceTabActive()
                .verifyLaptopNewDeviceTabInactive()

                .clickLaptopNewDeviceTab()
                .verifyPhoneNewDeviceTabInactive()
                .verifyTabletNewDeviceTabInactive()
                .verifyGadgetNewDeviceTabInactive()
                .verifyLaptopNewDeviceTabActive()

                .clickTabletNewDeviceTab()
                .verifyPhoneNewDeviceTabInactive()
                .verifyTabletNewDeviceTabActive()
                .verifyGadgetNewDeviceTabInactive()
                .verifyLaptopNewDeviceTabInactive()

                .clickTabletOldDeviceTab()
                .verifySmartphoneOldDeviceTabInactive()
                .verifyTabletOldDeviceTabActive()
                .verifyGadgetOldDeviceTabInactive()
                .verifyCameraOldDeviceTabInactive()
                .verifyTVOldDeviceTabInactive()
                .verifyLaptopOldDeviceTabInactive()
                .verifyConsoleOldDeviceTabInactive()

                .clickGadgetOldDeviceTab()
                .verifySmartphoneOldDeviceTabInactive()
                .verifyTabletOldDeviceTabInactive()
                .verifyGadgetOldDeviceTabActive()
                .verifyCameraOldDeviceTabInactive()
                .verifyTVOldDeviceTabInactive()
                .verifyLaptopOldDeviceTabInactive()
                .verifyConsoleOldDeviceTabInactive()

                .clickCameraOldDeviceTab()
                .verifySmartphoneOldDeviceTabInactive()
                .verifyTabletOldDeviceTabInactive()
                .verifyGadgetOldDeviceTabInactive()
                .verifyCameraOldDeviceTabActive()
                .verifyTVOldDeviceTabInactive()
                .verifyLaptopOldDeviceTabInactive()
                .verifyConsoleOldDeviceTabInactive()

                .clickTVOldDeviceTab()
                .verifySmartphoneOldDeviceTabInactive()
                .verifyTabletOldDeviceTabInactive()
                .verifyGadgetOldDeviceTabInactive()
                .verifyCameraOldDeviceTabInactive()
                .verifyTVOldDeviceTabActive()
                .verifyLaptopOldDeviceTabInactive()
                .verifyConsoleOldDeviceTabInactive()

                .clickLaptopOldDeviceTab()
                .verifySmartphoneOldDeviceTabInactive()
                .verifyTabletOldDeviceTabInactive()
                .verifyGadgetOldDeviceTabInactive()
                .verifyCameraOldDeviceTabInactive()
                .verifyTVOldDeviceTabInactive()
                .verifyLaptopOldDeviceTabActive()
                .verifyConsoleOldDeviceTabInactive()

                .clickNoDamageTitleLabel()
                .verifyNoDamageRadioCheckedDisplayed();
    }
}
