import org.example.HeaderActions;
import org.testng.annotations.Test;

public class HeaderTest {
    @Test
    public void ClickCatalogMenu() {
        HeaderActions action = new HeaderActions();
        for (int i = 0; i < 12; i++) {
            System.out.println(i);

            action
                    .clickCatalogMenu();
            switch (i) {
                case 0:
                    action
                            .clickTopSales();
                    break;
                case 1:
                    action
                            .clickPhones();
                    break;
                case 2:
                    action
                            .clickTelevisionsAndMultimedia();
                    break;
                case 3:
                    action
                            .clickHouseholdAppliances();
                    break;
                case 4:
                    action
                            .clickLaptopsPC();
                    break;
                case 5:
                    action
                            .clickApple();
                    break;
                case 6:
                    action
                            .clickXiaomi();
                    break;
                case 7:
                    action
                            .clickGadgetsAndElectricVehicles();
                    break;
                case 8:
                    action
                            .clickAudio();
                    break;
                case 9:
                    action
                            .clickSportAndHealth();
                    break;
                case 10:
                    action
                            .clickTourismAndFishing();
                    break;
                case 11:
                    action
                            .clickRepairAndEquipment();
                    break;
            }


        }
    }
}
