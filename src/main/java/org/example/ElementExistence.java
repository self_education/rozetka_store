package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.example.BeforeRun.getDriver;

public interface ElementExistence {

    default void verifyElementExists(String elementXpath) {
        getDriver().findElement(By.xpath(elementXpath)).isDisplayed();
    }

    default void verifyElementNotExists(String elementXpath) {
        try {
        WebElement element = getDriver().findElement(By.xpath(elementXpath));

            throw new AssertionError("ElementWasFoundException: " + elementXpath);
        } catch (org.openqa.selenium.NoSuchElementException ex) {}
    }
}
