package org.example;

public enum CommonConstants {

    ONE(1, "1", "one"),
    TWO(2, "2", "two"),
    THREE(3,"3", "three"),
    FOUR(4, "4", "four"),
    FIVE(5, "5", "five"),
    SIX(6, "6", "six"),
    SEVEN(7,"7", "seven"),
    EIGHT(8,"8", "eight"),
    NINE(9, "9", "nine"),
    TEN(10, "10", "ten");

    private final int intValue;
    private final String digitValue;
    private final String wordValue;

    CommonConstants(int intValue, String digitValue, String wordValue){
        this.intValue = intValue;
        this.digitValue = digitValue;
        this.wordValue = wordValue;
    }

    public int getInt() {
        return intValue;
    }

    public String getDigit() {
        return digitValue;
    }

    public String getWord() {
        return wordValue;
    }

}
