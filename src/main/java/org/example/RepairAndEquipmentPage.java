package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class RepairAndEquipmentPage extends HeaderPage {

    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Сантехніка']")
    WebElement plumbing;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Ремонт']")
    WebElement repair;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Електромонтажне обладнання']")
    WebElement electricalInstallationEquipment;

    public RepairAndEquipmentPage() {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

}
