package org.example;

public class XiaomiActions extends HeaderActions {

    XiaomiPage xiaomiPage = new XiaomiPage();

    public XiaomiActions() {
        UrlVerifier.UrlVerifier("https://allo.ua/ua/xiaomi-store/");
    }

    public XiaomiActions clickNextButton() {
        xiaomiPage.nextButton.click();
        return this;
    }

    public XiaomiActions clickBeforeButton() {
        xiaomiPage.beforeButton.click();
        return this;
    }
}
