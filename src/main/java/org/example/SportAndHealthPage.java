package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class SportAndHealthPage extends HeaderPage {

    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Велотовари та аксесуари']")
    WebElement bicyclesAndAccessories;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Скейтборди']")
    WebElement skateboards;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Деки для скейтбордів']")
    WebElement decksForSkateboards;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Колеса для скейтбордів']")
    WebElement wheelsForSkateboards;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Комплектуючі для скейтбордів']")
    WebElement accessoriesForSkateboards;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Одяг для схуднення']")
    WebElement slimmingClothes;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Тренажери']")
    WebElement simulators;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Спортивний інвентар']")
    WebElement sportsEquipment;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Єдиноборства']")
    WebElement martialArts;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Дартс']")
    WebElement darts;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Теніс і сквош']")
    WebElement tennisAndSquash;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Ролики і захист']")
    WebElement rollersAndProtection;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Зимовий спорт']")
    WebElement winterSports;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Водний спорт']")
    WebElement waterSports;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = " + "\"Здоров'я і догляд\"" + "]")
    WebElement healthAndCare;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Слухові апарати']")
    WebElement hearingAids;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Пульсометри та кардіодатчики']")
    WebElement cardiacSensors;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Рації']")
    WebElement walkieTalkies;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Аксесуари до рацій']")
    WebElement accessoriesForWalkieTalkies;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Баскетбол']")
    WebElement basketball;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Більярд']")
    WebElement billiards;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Міні-гольф']")
    WebElement miniGolf;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Захисне екіпірування']")
    WebElement protectiveEquipment;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Батути і аксесуари']")
    WebElement trampolinesAndAccessories;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Гімнастика']")
    WebElement gymnastics;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Хокей']")
    WebElement hockey;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Фігурне катання']")
    WebElement figureSkating;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Спортивні кубки']")
    WebElement sportsCups;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Спортивні медалі']")
    WebElement sportsMedals;

    public SportAndHealthPage() {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

}
