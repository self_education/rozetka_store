package org.example;

public enum ShipmentPaymentReferences {

    CALL_CENTRE_NUMBER("(0-800) 300-100"),
    CALL_CENTRE_WORKING_HOURS("з 07:00 до 23:00");

    private final String info;

    ShipmentPaymentReferences(String info){
        this.info = info;
    }

    public String getInfo() {
        return info;
    }
}
