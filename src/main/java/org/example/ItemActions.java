package org.example;

public class ItemActions extends HeaderActions {

    ItemPage itemPage = new ItemPage();

    public ItemActions verifyItemName(String name) {
        softAssert.assertTrue(name.equals(itemPage.title.getText()), "\n Wrong Item name" +
                "\n Current title is: " + itemPage.title.getText() +
                "\n Expected result: " + name + "\n");
        softAssert.assertAll();
        return this;
    }

    public ItemActions verifyTVType(String parameter) {
        softAssert.assertTrue(parameter.equals(itemPage.tvType.getText()), "\n Wrong Item name" +
                "\n Current title is: " + itemPage.tvType.getText() +
                "\n Expected result: " + parameter + "\n");
        softAssert.assertAll();
        return this;
    }

    public ItemActions verifyItemCode(String code) {
        softAssert.assertTrue(code.equals(itemPage.itemCode.getText()), "\n Wrong Item name" +
                "\n Current title is: " + itemPage.itemCode.getText() +
                "\n Expected result: " + code + "\n");
        softAssert.assertAll();
        return this;
    }

    public ItemActions verifyItemOutOfStock() {
        softAssert.assertTrue(ItemsTextDataBase.ITEM_OUT_OF_STOCK.getMessage().equals(itemPage.itemStockLabel.getText()), "'Out Of Stock' is not displayed: " + "\"" + itemPage.itemStockLabel.getText() + "\"");
        softAssert.assertAll();
        return this;
    }

    public ItemActions verifyItemInStock() {
        softAssert.assertTrue(ItemsTextDataBase.ITEM_IN_STOCK.getMessage().equals(itemPage.itemStockLabel.getText()), "'In Stock' is not displayed: " + "\"" + itemPage.itemStockLabel.getText() + "\"");
        softAssert.assertAll();
        return this;
    }
    public ItemActions verifyItemInAlloStores() {
        softAssert.assertTrue(ItemsTextDataBase.ITEM_IN_ALLO_STORES.getMessage().equals(itemPage.itemStockLabel.getText()), "'In Allo Stores' is not displayed: " + "\"" + itemPage.itemStockLabel.getText() + "\"");
        softAssert.assertAll();
        return this;
    }

}
