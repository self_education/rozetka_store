package org.example;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BeforeRun {
    public static WebDriver driver;
    private static String chromeOptions;

    @Test
    @Parameters({"baseURL", "chromeOptions", "dimensionWidth", "dimensionHeight"})
    public void setUp(String baseURL, String chromeOptions, int dimensionWidth, int dimensionHeight) {
        BeforeRun.chromeOptions = chromeOptions;
        WebDriverManager.chromedriver().clearDriverCache().setup();
        Dimension dimension = new Dimension(dimensionWidth, dimensionHeight);
        setDriver().manage().window().setSize(dimension);
        getDriver().get(baseURL);
    }

    public static WebDriver setDriver() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments(chromeOptions);
        driver = new ChromeDriver(options);
        return driver;
    }

    public static WebDriver getDriver() {
        return driver;
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }
}

