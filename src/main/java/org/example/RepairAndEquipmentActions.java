package org.example;

public class RepairAndEquipmentActions extends HeaderActions {

    RepairAndEquipmentPage repairAndEquipmentPage = new RepairAndEquipmentPage();

    public RepairAndEquipmentActions() {
        UrlVerifier.UrlVerifier("https://allo.ua/ua/santehnika-i-remont/");
    }


    public RepairAndEquipmentActions clickPlumbing() {
        repairAndEquipmentPage.plumbing.click();
        return this;
    }

    public RepairAndEquipmentActions clickRepair() {
        repairAndEquipmentPage.repair.click();
        return this;
    }

    public RepairAndEquipmentActions clickElectricalInstallationEquipment() {
        repairAndEquipmentPage.electricalInstallationEquipment.click();
        return this;
    }

    @Override
    public RepairAndEquipmentActions verifyHeaderUrl(String url) {
        UrlVerifier.UrlVerifier(url);
        return this;
    }
}
