package org.example;

public enum WarrantyAndServiceDataBase {

    RETURN_CHECK_REPAIR_REQUEST_STATUS_POP_UP_DESCRIPTION("Для того щоб перевірити статус Вашої заявки на ремонт введіть, будь ласка, номер «Акту приймання товару»"),
    RETURN_CHECK_REPAIR_REQUEST_NOT_FOUND_POP_UP_CODE("7777-777777"),
    RETURN_CHECK_REPAIR_REQUEST_VALIDATION_TEXT("Це поле є обов'язковим для заповнення."),
    RETURN_CHECK_REPAIR_REQUEST_NUMBERS_ONLY_TEXT("В поле дозволено тільки числове значення"),
    RETURN_CHECK_REPAIR_REQUEST_NOT_FOUND_POP_UP_TEXT("По введеному номеру акту заявка не знайдена"),
    RETURN_CHECK_REPAIR_REQUEST_CONTACT_INFO_MESSAGE("З будь-яких додаткових питань будемо раді вам відповісти по телефону нашої гарячої лінії 0 800 300 100"),
    RETURN_CHECK_RETURN_FAQS_NOT_FIT("Що робити, якщо товар не підійшов за формою, кольором, розміром або з іншої причини?"),
    RETURN_CHECK_RETURN_FAQS_CANNOT_BE_RETURNED("Не всі товари можна повернути. Це правда?"),
    RETURN_CHECK_RETURN_FAQS_CHECK_LOST("Що робити, якщо не збережено чек / акт приймання-передачі / видаткову накладну?"),
    RETURN_CHECK_RETURN_FAQS_WARRANTY_PERIOD("Який гарантійний строк у мого товару?"),
    RETURN_CHECK_RETURN_FAQS_BREAKS_DOWN_DURING_THE_WARRANTY_PERIOD("Що робити, якщо товар вийшов з ладу впродовж встановленого гарантійного строку?"),
    RETURN_CHECK_RETURN_FAQS_VERIFY_XIAOMI_IMEI("Перевірка IMEI чи серійного номеру (S/N) Xiaomi"),

    REVIEWS_QUESTION_ONE_HEADER("1. Наскільки ви готові рекомендувати сервісний центр?"),
    REVIEWS_QUESTION_TWO_HEADER("2. Чи вирішена причина вашого звернення?"),
    REVIEWS_QUESTION_THREE_HEADER("3. Надане рішення відповідає вашим очікуванням?"),
    REVIEWS_QUESTION_FOUR_HEADER("4. Наскільки комфортно було надано обслуговування при передачі товару на сервісне обслуговування?"),
    NUMBER_BACKGROUND_IF_ACTIVE_COLOR("rgba(51, 62, 72, 1)"),
    NUMBER_BACKGROUND_IF_NOT_ACTIVE_COLOR("rgba(255, 255, 255, 1)");
    private final String text;

    WarrantyAndServiceDataBase(String text) {
        this.text = text;
    }

    public String getMessage() {
        return text;
    }

}
