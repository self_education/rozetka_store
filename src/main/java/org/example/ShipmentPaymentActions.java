package org.example;

public class ShipmentPaymentActions extends HeaderActions {

    ShipmentPaymentPage shipmentPaymentPage = new ShipmentPaymentPage();

    public ShipmentPaymentActions verifyHowToPlaceAnOrderHeaderVisible() {
        waitVisible(shipmentPaymentPage.howToPlaceAnOrderHeader);
        return this;
    }

    public ShipmentPaymentActions verifyHowToPlaceAnOrderInfoVisible() {
        waitVisible(shipmentPaymentPage.howToPlaceAnOrderInfo);
        return this;
    }

    public ShipmentPaymentActions verifyCallCentreNumberCorrect() {
        softAssert.assertTrue(shipmentPaymentPage.callCentreNumber.getText().equals(ShipmentPaymentReferences.CALL_CENTRE_NUMBER.getInfo()), "\n Call-Centre number is not correct: " +
                "\n Current number is: " + shipmentPaymentPage.callCentreNumber.getText() +
                "\n Expected result: " + ShipmentPaymentReferences.CALL_CENTRE_NUMBER.getInfo() + "\n");
        softAssert.assertAll();
        return this;
    }

    public ShipmentPaymentActions verifyCallCentreWorkingHoursCorrect() {
        softAssert.assertTrue(shipmentPaymentPage.callCentreWorkingHours.getText().equals(ShipmentPaymentReferences.CALL_CENTRE_WORKING_HOURS.getInfo()), "\n Call-Centre working hours is not correct: " +
                "\n Current working hours is: " + shipmentPaymentPage.callCentreWorkingHours.getText() +
                "\n Expected result: " + ShipmentPaymentReferences.CALL_CENTRE_WORKING_HOURS.getInfo() + "\n");
        softAssert.assertAll();
        return this;
    }

    public ShipmentPaymentActions clickHowGetDButton() {
        shipmentPaymentPage.howGetDButton.click();
        return this;
    }

    public ShipmentPaymentActions clickWhenButton() {
        shipmentPaymentPage.whenButton.click();
        return this;
    }

    public ShipmentPaymentActions clickPriceButton() {
        shipmentPaymentPage.priceButton.click();
        return this;
    }

    public ShipmentPaymentActions clickPaymentButton() {
        shipmentPaymentPage.payButton.click();
        return this;
    }

    public ShipmentPaymentActions clickCheckButton() {
        shipmentPaymentPage.checkButton.click();
        return this;
    }

    public ShipmentPaymentActions verifyHowTOGetProductVisible() {
        waitVisible(shipmentPaymentPage.howTOGetProduct);
        return this;
    }

    public ShipmentPaymentActions verifyPickupFromTheStoreVisible() {
        waitVisible(shipmentPaymentPage.pickupFromTheStore);
        return this;
    }

    public ShipmentPaymentActions verifyDeliveryToTheApartmentVisible() {
        waitVisible(shipmentPaymentPage.deliveryToTheApartment);
        return this;
    }

    public ShipmentPaymentActions verifyPickupFromNovaPoshtaVisible() {
        waitVisible(shipmentPaymentPage.pickupFromNovaPoshta);
        return this;
    }

    public ShipmentPaymentActions verifyPickupFromMeestPostVisible() {
        waitVisible(shipmentPaymentPage.pickupFromMeestPost);
        return this;
    }

    public ShipmentPaymentActions verifyDeliveryByMeestPostVisible() {
        waitVisible(shipmentPaymentPage.deliveryByMeestPost);
        return this;
    }

    public ShipmentPaymentActions verifyDeliveryByNovaPoshtaVisible() {
        waitVisible(shipmentPaymentPage.deliveryByNovaPoshta);
        return this;
    }

    public ShipmentPaymentActions verifyNovaPoshtaImgVisible() {
        waitVisible(shipmentPaymentPage.novaPoshtaImg);
        return this;
    }

    public ShipmentPaymentActions verifyMeestImgVisible() {
        waitVisible(shipmentPaymentPage.meestImg);
        return this;
    }

    public ShipmentPaymentActions verifyDeliveryInfoVisible() {
        waitVisible(shipmentPaymentPage.deliveryInfo);
        return this;
    }

    public ShipmentPaymentActions verifyCashlessPaymentVisible() {
        waitVisible(shipmentPaymentPage.paymentCashless);
        return this;
    }

    public ShipmentPaymentActions verifyCashPaymentVisible() {
        waitVisible(shipmentPaymentPage.paymentCash);
        return this;
    }

    public ShipmentPaymentActions verifyCardPaymentVisible() {
        waitVisible(shipmentPaymentPage.paymentCard);
        return this;
    }

    public ShipmentPaymentActions verifyRecipientsRightsVisible() {
        waitVisible(shipmentPaymentPage.recipientsRights);
        return this;
    }

    public ShipmentPaymentActions verifyExaminationPhoneVisible() {
        waitVisible(shipmentPaymentPage.examinationPhone);
        return this;
    }

    public ShipmentPaymentActions verifyExaminationLaptopsVisible() {
        waitVisible(shipmentPaymentPage.examinationLaptops);
        return this;
    }

    public ShipmentPaymentActions verifyExaminationTVsVisible() {
        waitVisible(shipmentPaymentPage.examinationTVs);
        return this;
    }

    public ShipmentPaymentActions verifyExaminationPhotoVideoEquipmentVisible() {
        waitVisible(shipmentPaymentPage.examinationPhotoVideoEquipment);
        return this;
    }

    public ShipmentPaymentActions verifyExaminationHVACVisible() {
        waitVisible(shipmentPaymentPage.examinationHVAC);
        return this;
    }

    public ShipmentPaymentActions verifyExaminationSmallAppliancesVisible() {
        waitVisible(shipmentPaymentPage.examinationSmallAppliances);
        return this;
    }

    public ShipmentPaymentActions verifyAdditionalQuestionsParagraphVisible() {
        waitVisible(shipmentPaymentPage.additionalQuestionsParagraph);
        return this;
    }
}
