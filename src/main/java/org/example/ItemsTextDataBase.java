package org.example;

public enum ItemsTextDataBase {

    ITEM_OUT_OF_STOCK("Немає в наявності"),
    ITEM_IN_STOCK("✓ Товар в наявності"),
    ITEM_IN_ALLO_STORES("✓ У магазинах Алло");
    private final String text;

    ItemsTextDataBase(String text) {
        this.text = text;
    }

    public String getMessage() {
        return text;
    }

}
