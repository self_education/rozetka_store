package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;


public class TelevisionsPage extends HeaderPage {

    @FindBy(xpath = "//div[@class = 'accordion__header' and ./h3[.='Тип телевізора']]/following-sibling::div//a[text()[normalize-space()='LED']]")
    WebElement ledTelevisionType;
    @FindBy(xpath = "//div[@class = 'accordion__header' and ./h3[.='Тип телевізора']]/following-sibling::div//a[text()[normalize-space()='QLED']]")
    WebElement qledTelevisionType;
    @FindBy(xpath = "//span[@class = 'f-popup__message' and contains(text(), 'Показати')]")
    WebElement showButton;

    public TelevisionsPage() {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

}
