package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class ApplePage extends BasePage {

    @FindBy(xpath = "//ul[@id = 'menu']//a[normalize-space() = 'Mac']")
     WebElement menuMac;
    @FindBy(xpath = "//div[.='MacBook Air 15” new']/preceding-sibling::div/div[@class='wrapper-svg-size']")
     WebElement macBookAir15Img;
    @FindBy(xpath = "//ul[@id = 'menu']//a[normalize-space() = 'iPad']")
     WebElement menuIPad;
    @FindBy(xpath = "//ul[@id = 'menu']//a[normalize-space() = 'iPhone']")
     WebElement menuIPhone;
    @FindBy(xpath = "//ul[@id = 'menu']//a[normalize-space() = 'Watch']")
     WebElement menuWatch;
    @FindBy(xpath = "//ul[@id = 'menu']//a[normalize-space() = 'TV']")
     WebElement menuTV;
    @FindBy(xpath = "//ul[@id = 'menu']//a[normalize-space() = 'AirTag']")
     WebElement menuAirTag;
    @FindBy(xpath = "//ul[@id = 'menu']//a[normalize-space() = 'Music']")
     WebElement menuMusic;
    @FindBy(xpath = "//div[@class = 'l-description']//strong[. = 'MacBook Pro']")
     WebElement descriptionMac;
    @FindBy(xpath = "//div[@class = 'l-description']//strong[. = 'iPad']")
     WebElement descriptionIPad;
    @FindBy(xpath = "//div[@class = 'l-description']//strong[. = 'iPhone']")
     WebElement descriptionIPhone;
    @FindBy(xpath = "//div[@class = 'l-description']//strong[. = 'Apple Watch']")
     WebElement descriptionWatch;
    @FindBy(xpath = "//div[@class = 'l-description']//strong[. = 'Apple TV']")
     WebElement descriptionTV;
    @FindBy(xpath = "//div[@class = 'l-description']//strong[. = 'AirTag']")
     WebElement descriptionAirTag;
    @FindBy(xpath = "//div[@class = 'l-description l-description-custom']//strong[. = 'AirPods Pro']")
     WebElement descriptionMusic;
    @FindBy(xpath = "//div[@class = 'footer__nav-list']")
     WebElement footer;


    public ApplePage() {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }
}
