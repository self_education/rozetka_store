package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;


public class LaptopsPCPage extends HeaderPage {

    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Планшети']")
    WebElement tablets;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Ноутбуки']")
    WebElement laptops;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Мережеве обладнання']")
    WebElement networkEquipment;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Монітори']")
    WebElement Monitors;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = " + "\"Комп'ютери\"" + "]")
    WebElement personalComputers;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Моноблоки']")
    WebElement monoBlocks;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Неттопи']")
    WebElement netTops;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Аксесуари для планшетів']")
    WebElement accessoriesForTablets;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Аксесуари для ноутбуків та ПК']")
    WebElement accessoriesForLaptops;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Периферія для ПК']")
    WebElement peripherals;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Електронні книги']")
    WebElement e_Books;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Графічні планшети']")
    WebElement graphicTablets;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Аксесуари для графічних планшетів']")
    WebElement accessoriesForGraphicTablets;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Комплектуючі для ноутбуків']")
    WebElement laptopParts;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Комплектуючі для ПК']")
    WebElement personalComputerParts;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Аксесуари для корпусів ПК']")
    WebElement accessoriesForPCCases;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Сервери']")
    WebElement servers;

    public LaptopsPCPage() {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

}
