package org.example;

public class TradeInActions extends HeaderActions{

    TradeInActions(){
        UrlVerifier.UrlVerifier("https://allo.ua/ua/tradein/");
    }

    TradeInPage tradeInPage = new TradeInPage();

    public TradeInActions verifyBreadcrumbsTradeInIsDisplayed(){
        tradeInPage.breadcrumbsTradeIn.isDisplayed();
        return this;
    }

    public TradeInActions verifyThreeStepsForTradeInIsDisplayed(){
        tradeInPage.threeStepsForTradeIn.isDisplayed();
        return this;
    }

    public TradeInActions verifyFirstStepHeaderIsDisplayed(){
        tradeInPage.firstStepHeader.isDisplayed();
        return this;
    }

    public TradeInActions verifyPhoneNewDeviceTabActive(){
        tradeInPage.phoneNewDeviceTabActive.isDisplayed();
        return this;
    }

    public TradeInActions verifyTabletNewDeviceTabActive(){
        tradeInPage.tabletNewDeviceTabActive.isDisplayed();
        return this;
    }

    public TradeInActions verifyGadgetNewDeviceTabActive(){
        tradeInPage.gadgetNewDeviceTabActive.isDisplayed();
        return this;
    }

    public TradeInActions verifyLaptopNewDeviceTabActive(){
        tradeInPage.laptopNewDeviceTabActive.isDisplayed();
        return this;
    }

    public TradeInActions verifyPhoneNewDeviceTabInactive(){
        tradeInPage.phoneNewDeviceTabInactive.isDisplayed();
        return this;
    }

    public TradeInActions verifyTabletNewDeviceTabInactive(){
        tradeInPage.tabletNewDeviceTabInactive.isDisplayed();
        return this;
    }

    public TradeInActions verifyGadgetNewDeviceTabInactive(){
        tradeInPage.gadgetNewDeviceTabInactive.isDisplayed();
        return this;
    }

    public TradeInActions verifyLaptopNewDeviceTabInactive(){
        tradeInPage.laptopNewDeviceTabInactive.isDisplayed();
        return this;
    }

    public TradeInActions verifySmartphoneOldDeviceTabActive(){
        tradeInPage.smartphoneOldDeviceTabActive.isDisplayed();
        return this;
    }

    public TradeInActions verifyTabletOldDeviceTabActive(){
        tradeInPage.tabletOldDeviceTabActive.isDisplayed();
        return this;
    }

    public TradeInActions verifyGadgetOldDeviceTabActive(){
        tradeInPage.gadgetOldDeviceTabActive.isDisplayed();
        return this;
    }

    public TradeInActions verifyCameraOldDeviceTabActive(){
        tradeInPage.cameraOldDeviceTabActive.isDisplayed();
        return this;
    }

    public TradeInActions verifyTVOldDeviceTabActive(){
        tradeInPage.tvOldDeviceTabActive.isDisplayed();
        return this;
    }

    public TradeInActions verifyLaptopOldDeviceTabActive(){
        tradeInPage.laptopOldDeviceTabActive.isDisplayed();
        return this;
    }

    public TradeInActions verifyConsoleOldDeviceTabActive(){
        tradeInPage.consoleOldDeviceTabActive.isDisplayed();
        return this;
    }

    public TradeInActions verifySmartphoneOldDeviceTabInactive(){
        tradeInPage.smartphoneOldDeviceTabInactive.isDisplayed();
        return this;
    }

    public TradeInActions verifyTabletOldDeviceTabInactive(){
        tradeInPage.tabletOldDeviceTabInactive.isDisplayed();
        return this;
    }

    public TradeInActions verifyGadgetOldDeviceTabInactive(){
        tradeInPage.gadgetOldDeviceTabInactive.isDisplayed();
        return this;
    }

    public TradeInActions verifyCameraOldDeviceTabInactive(){
        tradeInPage.cameraOldDeviceTabInactive.isDisplayed();
        return this;
    }

    public TradeInActions verifyTVOldDeviceTabInactive(){
        tradeInPage.tvOldDeviceTabInactive.isDisplayed();
        return this;
    }

    public TradeInActions verifyLaptopOldDeviceTabInactive(){
        tradeInPage.laptopOldDeviceTabInactive.isDisplayed();
        return this;
    }

    public TradeInActions verifyConsoleOldDeviceTabInactive(){
        tradeInPage.consoleOldDeviceTabInactive.isDisplayed();
        return this;
    }

    public TradeInActions clickPhoneNewDeviceTab(){
        waitAppear(tradeInPage.spinnerNewDevice);
        tradeInPage.phoneNewDeviceTab.click();
        return this;
    }

    public TradeInActions clickTabletNewDeviceTab(){
        waitAppear(tradeInPage.spinnerNewDevice);
        tradeInPage.tabletNewDeviceTab.click();
        return this;
    }

    public TradeInActions clickGadgetNewDeviceTab(){
        waitAppear(tradeInPage.spinnerNewDevice);
        tradeInPage.gadgetNewDeviceTab.click();
        return this;
    }

    public TradeInActions clickLaptopNewDeviceTab(){
        waitAppear(tradeInPage.spinnerNewDevice);
        tradeInPage.laptopNewDeviceTab.click();
        return this;
    }

    public TradeInActions clickSmartphoneOldDeviceTab(){
        waitAppear(tradeInPage.spinnerOldDevice);
        tradeInPage.smartphoneOldDeviceTab.click();
        return this;
    }

    public TradeInActions clickTabletOldDeviceTab(){
        waitAppear(tradeInPage.spinnerOldDevice);
        tradeInPage.tabletOldDeviceTab.click();
        return this;
    }

    public TradeInActions clickGadgetOldDeviceTab(){
        waitAppear(tradeInPage.spinnerOldDevice);
        tradeInPage.gadgetOldDeviceTab.click();
        return this;
    }

    public TradeInActions clickCameraOldDeviceTab(){
        waitAppear(tradeInPage.spinnerOldDevice);
        tradeInPage.cameraOldDeviceTab.click();
        return this;
    }

    public TradeInActions clickTVOldDeviceTab(){
        waitAppear(tradeInPage.spinnerOldDevice);
        tradeInPage.tvOldDeviceTab.click();
        return this;
    }

    public TradeInActions clickLaptopOldDeviceTab(){
        waitAppear(tradeInPage.spinnerOldDevice);
        tradeInPage.laptopOldDeviceTab.click();
        return this;
    }

    public TradeInActions clickNoDamageTitleLabel(){
        tradeInPage.noDamageTitle.click();
        return this;
    }

    public TradeInActions verifyNoDamageRadioCheckedDisplayed(){
        tradeInPage.noDamageRadioChecked.isDisplayed();
        return this;
    }

}
