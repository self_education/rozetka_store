package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class TradeInPage extends HeaderPage {

    @FindBy(xpath = "//div/div[preceding-sibling::div[@class='snap-slider__item']/a[.='Інтернет магазин']]/span[@class = 'b-crumbs__link' and .='Поміняй свій старий телефон на новий!']")
    WebElement breadcrumbsTradeIn;
    @FindBy(xpath = "//div/div[.='Виберіть модель нового гаджета' and following-sibling::div[.= 'Вкажіть модель свого старого гаджета та дізнайтеся його оціночну вартість'and following-sibling::div[.='Оформіть заявку на АЛЛО Обмін, Обміняйте в магазині АЛЛО']]]")
    WebElement threeStepsForTradeIn;
    @FindBy(xpath = "//span[@class = 'step-text' and .= 'Я хочу купити новий пристрій']")
    WebElement firstStepHeader;
    @FindBy(xpath = "//div[@x-data = 'newDevice']//div[contains(@class, 'exchange-tab tab-btn no-wrap active') and .= 'Мобільний телефон']")
    WebElement phoneNewDeviceTabActive;
    @FindBy(xpath = "//div[@x-data = 'newDevice']//div[contains(@class, 'exchange-tab tab-btn no-wrap active') and .= 'Планшет']")
    WebElement tabletNewDeviceTabActive;
    @FindBy(xpath = "//div[@x-data = 'newDevice']//div[contains(@class, 'exchange-tab tab-btn no-wrap active') and .= 'Гаджет']")
    WebElement gadgetNewDeviceTabActive;
    @FindBy(xpath = "//div[@x-data = 'newDevice']//div[contains(@class, 'exchange-tab tab-btn no-wrap active') and .= 'Ноутбук']")
    WebElement laptopNewDeviceTabActive;
    @FindBy(xpath = "//div[@class = 'exchange-tab tab-btn no-wrap' and .= 'Мобільний телефон']")
    WebElement phoneNewDeviceTabInactive;
    @FindBy(xpath = "//div[@x-data = 'newDevice']//div[@class = 'exchange-tab tab-btn no-wrap' and .= 'Планшет']")
    WebElement tabletNewDeviceTabInactive;
    @FindBy(xpath = "//div[@x-data = 'newDevice']//div[@class = 'exchange-tab tab-btn no-wrap' and .= 'Гаджет']")
    WebElement gadgetNewDeviceTabInactive;
    @FindBy(xpath = "//div[@x-data = 'newDevice']//div[@class = 'exchange-tab tab-btn no-wrap' and .= 'Ноутбук']")
    WebElement laptopNewDeviceTabInactive;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[contains(@class, 'exchange-tab tab-btn no-wrap active') and .= 'Смартфон']")
    WebElement smartphoneOldDeviceTabActive;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[contains(@class, 'exchange-tab tab-btn no-wrap active') and .= 'Планшет']")
    WebElement tabletOldDeviceTabActive;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[contains(@class, 'exchange-tab tab-btn no-wrap active') and .= 'Гаджет']")
    WebElement gadgetOldDeviceTabActive;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[contains(@class, 'exchange-tab tab-btn no-wrap active') and .= 'Фотоапарат']")
    WebElement cameraOldDeviceTabActive;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[contains(@class, 'exchange-tab tab-btn no-wrap active') and .= 'Телевізор']")
    WebElement tvOldDeviceTabActive;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[contains(@class, 'exchange-tab tab-btn no-wrap active') and .= 'Ноутбук']")
    WebElement laptopOldDeviceTabActive;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[contains(@class, 'exchange-tab tab-btn no-wrap active') and .= 'Ігрова консоль']")
    WebElement consoleOldDeviceTabActive;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[@class = 'exchange-tab tab-btn no-wrap' and .= 'Смартфон']")
    WebElement smartphoneOldDeviceTabInactive;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[@class = 'exchange-tab tab-btn no-wrap' and .= 'Планшет']")
    WebElement tabletOldDeviceTabInactive;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[@class = 'exchange-tab tab-btn no-wrap' and .= 'Гаджет']")
    WebElement gadgetOldDeviceTabInactive;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[@class = 'exchange-tab tab-btn no-wrap' and .= 'Фотоапарат']")
    WebElement cameraOldDeviceTabInactive;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[@class = 'exchange-tab tab-btn no-wrap' and .= 'Телевізор']")
    WebElement tvOldDeviceTabInactive;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[@class = 'exchange-tab tab-btn no-wrap' and .= 'Ноутбук']")
    WebElement laptopOldDeviceTabInactive;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[@class = 'exchange-tab tab-btn no-wrap' and .= 'Ігрова консоль']")
    WebElement consoleOldDeviceTabInactive;
    @FindBy(xpath = "//div[@x-data = 'newDevice']//div[ .= 'Мобільний телефон']")
    WebElement phoneNewDeviceTab;
    @FindBy(xpath = "//div[@x-data = 'newDevice']//div[ .= 'Планшет']")
    WebElement tabletNewDeviceTab;
    @FindBy(xpath = "//div[@x-data = 'newDevice']//div[ .= 'Гаджет']")
    WebElement gadgetNewDeviceTab;
    @FindBy(xpath = "//div[@x-data = 'newDevice']//div[ .= 'Ноутбук']")
    WebElement laptopNewDeviceTab;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[.= 'Смартфон']")
    WebElement smartphoneOldDeviceTab;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[.= 'Планшет']")
    WebElement tabletOldDeviceTab;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[.= 'Гаджет']")
    WebElement gadgetOldDeviceTab;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[.= 'Фотоапарат']")
    WebElement cameraOldDeviceTab;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[.= 'Телевізор']")
    WebElement tvOldDeviceTab;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[.= 'Ноутбук']")
    WebElement laptopOldDeviceTab;
    @FindBy(xpath = "//div[@x-data = 'newDevice']//div[.= 'Виберіть виробника']")
    WebElement manufacturerNewDeviceTitle;
    @FindBy(xpath = "//div[@x-data = 'oldDevice']//div[.= 'Виберіть виробника']")
    WebElement manufacturerOldDeviceTitle;
    @FindBy(xpath = "//div[@class='device-state__info']/div[. = 'Немає видимих пошкоджень']")
    WebElement noDamageTitle;
    @FindBy(xpath = "//div[@class='device-state__info' and ./div[. = 'Немає видимих пошкоджень']]/preceding-sibling::input[@checked = 'checked']")
    WebElement noDamageRadioChecked;


    By spinnerNewDevice = By.xpath("//div[@x-data = 'newDevice']//div[@class='spinner' and @style='display: none;']");
    By spinnerOldDevice = By.xpath( "//div[@x-data = 'oldDevice']//div[@class='spinner' and @style='display: none;']");



    TradeInPage(){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }


}
