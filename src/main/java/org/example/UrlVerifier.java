package org.example;

import static org.example.BeforeRun.getDriver;

public interface UrlVerifier {

    static void UrlVerifier(String url){
        if(!getDriver().getCurrentUrl().equals(url))
            throw new IllegalStateException("\n Wrong page URL link" +
                    " current page is: " + getDriver().getCurrentUrl() +
                    "\n Expected result: " + url);
    }

}
