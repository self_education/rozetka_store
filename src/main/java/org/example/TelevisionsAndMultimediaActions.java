package org.example;

public class TelevisionsAndMultimediaActions extends HeaderActions {

    TelevisionsAndMultimediaPage televisionsAndMultimediaPage = new TelevisionsAndMultimediaPage();

    public TelevisionsAndMultimediaActions() {
        UrlVerifier.UrlVerifier("https://allo.ua/ua/televizory-i-mediapleery/");
    }

    public TelevisionsActions clickTelevisions() {
        televisionsAndMultimediaPage.televisions.click();
        return new TelevisionsActions();
    }

    public TelevisionsAndMultimediaActions clickDigitalPhotoFrames() {
        televisionsAndMultimediaPage.digitalPhotoFrames.click();
        return this;
    }

    public TelevisionsAndMultimediaActions clickProjectionEquipment() {
        televisionsAndMultimediaPage.projectionEquipment.click();
        return this;
    }

    public TelevisionsAndMultimediaActions clickPortableTVs() {
        televisionsAndMultimediaPage.portableTVs.click();
        return this;
    }

    public TelevisionsAndMultimediaActions clickPhotoAndVideoEquipment() {
        televisionsAndMultimediaPage.photoAndVideoEquipment.click();
        return this;
    }

    public TelevisionsAndMultimediaActions clickPlayersMP3() {
        televisionsAndMultimediaPage.playersMP3.click();
        return this;
    }

    @Override
    public TelevisionsAndMultimediaActions verifyHeaderUrl(String url) {
        UrlVerifier.UrlVerifier(url);
        return this;
    }

}
