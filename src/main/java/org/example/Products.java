package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.example.BeforeRun.getDriver;

public interface Products extends ElementExistence {

    WebDriverWait wait = new WebDriverWait(getDriver(), 2000);

    default WebElement findProductByName(String name) {
        WebElement product = getDriver().findElement(By.xpath("//div[@class='product-card__content']/a[.='" + name + "']"));
        return product;
    }

    default <T> T clickProductByName(String name) {
        wait.until(ExpectedConditions.elementToBeClickable(findProductByName(name)));
        findProductByName(name).click();
        return (T) this;
    }

    default <T> T verifyProductExists(String name, Class<T> className) {
        verifyElementExists("//div[@class='product-card__content']/a[.='" + name + "']");
        return (T) this;
    }

    default <T> T verifyProductNotExists(String name, Class<T> className) {
        verifyElementNotExists("//div[@class='product-card__content']/a[.='" + name + "']");
        return (T) this;
    }


}
