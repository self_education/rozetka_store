package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;


public class TelevisionsAndMultimediaPage extends HeaderPage {
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Телевізори']")
    WebElement televisions;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Цифрові фоторамки']")
    WebElement digitalPhotoFrames;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Проекційне обладнання']")
    WebElement projectionEquipment;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Портативні телевізори']")
    WebElement portableTVs;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Фото та відеотехніка']")
    WebElement photoAndVideoEquipment;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'MP3-плеєри']")
    WebElement playersMP3;

    public TelevisionsAndMultimediaPage() {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

}
