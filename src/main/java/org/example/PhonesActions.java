package org.example;

public class PhonesActions extends HeaderActions {

    PhonesPage phones = new PhonesPage();

    public PhonesActions() {
        UrlVerifier.UrlVerifier("https://allo.ua/ua/mobilnye-telefony-i-sredstva-svyazi/");
    }

    public PhonesActions clickSmartphones() {
        phones.smartphone.click();
        return this;
    }

    public PhonesActions clickMobile() {
        phones.phone.click();
        return this;
    }

    public PhonesActions clickPhoneForHome() {
        phones.phoneForHome.click();
        return this;
    }

    public PhonesActions clickPhoneAccessories() {
        phones.phoneAccessories.click();
        return this;
    }

    public PhonesActions clickPhoneSpareParts() {
        phones.phoneSpareParts.click();
        return this;
    }

    @Override
    public PhonesActions verifyHeaderUrl(String url) {
        UrlVerifier.UrlVerifier(url);
        return this;
    }
}
