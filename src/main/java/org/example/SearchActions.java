package org.example;

import org.openqa.selenium.By;

public class SearchActions extends HeaderActions {

    SearchPage searchPage = new SearchPage();

    public SearchActions verifyCatalogEmpty() {
        softAssert.assertTrue(searchPage.catalogEmpty.isDisplayed(), "Item is not displayed.");
        softAssert.assertAll();
        return this;
    }

    public Integer itemsFound(String itemName) {
        String founded = searchPage.itemsFound.getText();
        StringBuffer stringBuffer = new StringBuffer(founded);

        String prefixToRemove = "Результати пошуку для '" + itemName + "'. Знайдено товарів: ";
        int index = stringBuffer.indexOf(prefixToRemove);
        stringBuffer.delete(index, index + prefixToRemove.length());
        return Integer.valueOf(stringBuffer.toString());
    }

    public SearchActions verifyItemsFoundEquality(Integer amount, String itemName) {
        waitAppear(By.xpath("//span[contains(., '" + itemName + "')]"));
        softAssert.assertTrue(itemsFound(itemName).equals(amount), "\n Amount of items not equal: " +
                "\n Current amount is: " + itemsFound(itemName) +
                "\n Expected result: " + amount + "\n");
        softAssert.assertAll();
        return this;
    }

}
