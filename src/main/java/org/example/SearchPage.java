package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class SearchPage extends HeaderPage{

    @FindBy(xpath = "//p[@class = 'v-catalog__empty']")
    WebElement catalogEmpty;
    @FindBy(xpath = "//span[text()[contains(., 'Знайдено товарів:')]]")
    WebElement itemsFound;

    public SearchPage(){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

}
