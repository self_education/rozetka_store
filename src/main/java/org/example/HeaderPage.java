package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class HeaderPage extends BasePage {

    @FindBy(xpath = "//div[@class = 'ct-button']")
     WebElement catalogMenu;
    @FindBy(xpath = "//a[@class = 'mm__a' and normalize-space()='Тримай заряд']")
     WebElement holdCharge;
    @FindBy(xpath = "//a[normalize-space()='Топ продаж']")
     WebElement topSales;
    @FindBy(xpath = "//a[@class = 'mm__a' and normalize-space()='Смартфони та телефони']")
     WebElement phones;
    @FindBy(xpath = "//a[@class = 'mm__a' and normalize-space()='Телевізори та мультимедіа']")
     WebElement televisionsAndMultimedia;
    @FindBy(xpath = "//a[@class = 'mm__a' and normalize-space()='Побутова техніка']")
     WebElement householdAppliances;
    @FindBy(xpath = "//a[@class = 'mm__a' and normalize-space()='Ноутбуки, ПК та планшети']")
     WebElement laptopsPC;
    @FindBy(xpath = "//a[@class = 'mm__a' and normalize-space()='Apple']")
     WebElement apple;
    @FindBy(xpath = "//a[@class = 'mm__a' and normalize-space()='Xiaomi']")
     WebElement xiaomi;
    @FindBy(xpath = "//a[@class = 'mm__a' and normalize-space()='Гаджети та електротранспорт']")
     WebElement gadgetsAndElectricVehicles;
    @FindBy(xpath = "//a[@class = 'mm__a' and normalize-space()='Аудіо']")
     WebElement audio;
    @FindBy(xpath = "//a[@class = 'mm__a' and normalize-space()=" + "\"Спорт і здоров'я\"" + "]")
     WebElement sportAndHealth;
    @FindBy(xpath = "//a[@class = 'mm__a' and normalize-space()='Туризм та риболовля']")
     WebElement tourismAndFishing;
    @FindBy(xpath = "//a[@class = 'mm__a' and normalize-space()='Ремонт та обладнання']")
     WebElement repairAndEquipment;

    @FindBy(xpath = "//input[@id = 'search-form__input']")
     WebElement searchItem;
    @FindBy(xpath = "//li[@class = 'search-models__items']/a")
     WebElement item;
    @FindBy(xpath = "//button[normalize-space() = 'Всі результати пошуку']")
    WebElement allResults;
    @FindBy(xpath = "//a[. = 'Доставка та оплата']")
    WebElement shipmentPayment;
    @FindBy(xpath = "//a[. = 'Гарантія та сервіс']")
    WebElement warrantyAndService;
    @FindBy(xpath = "//a[@class='mh-button' and normalize-space()='АЛЛО Обмін']")
    WebElement tradeIn;

     public HeaderPage() {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }
}
