package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class GadgetsAndElectricVehiclesPage extends HeaderPage {
@FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = '3D-друк']")
    WebElement print_3D;
@FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Фітнес-браслети']")
    WebElement fitnessBracelets;
@FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Аксесуари до смарт-годинників']")
    WebElement accessoriesForSmartwatches;
@FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Електротранспорт']")
    WebElement electricTransport;
@FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Ігрова зона']")
    WebElement gameZone;
@FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Екшн-камери і аксесуари']")
    WebElement actionCamerasAndAccessories;
@FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Системи нагрівання тютюну']")
    WebElement tobaccoHeatingSystems;
@FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Смарт-брелоки']")
    WebElement smartKeyrings;
@FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Смарт-кільця']")
    WebElement smartRings;
    public GadgetsAndElectricVehiclesPage() {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

}
