package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class ShipmentPaymentPage extends HeaderPage {

    @FindBy(xpath = "//h3[@class='sub-block-header' and .= 'Як оформити замовлення?']")
    WebElement howToPlaceAnOrderHeader;
    @FindBy(xpath = "//p[contains(normalize-space(), \"З будь-яких питань ви завжди можете зв'язатися з нами за телефоном 0(800)300-100 (безкоштовно) або напишіть нам в чат, ми дуже скоро вам відповімо.\")]")
    WebElement howToPlaceAnOrderInfo;
    @FindBy(xpath = "//h3[@class = 'sp-phones sp-call-center' and following-sibling::p[text() = 'По Україні']]")
    WebElement callCentreNumber;
    @FindBy(xpath = "//h3[@class = 'sp-phones sp-call-center' and following-sibling::p[text() = 'Щоденно']]")
    WebElement callCentreWorkingHours;
    @FindBy(xpath = "//button[@id='howGetd']")
    WebElement howGetDButton;
    @FindBy(xpath = "//button[@class='sp-tablinks' and .='Коли я отримаю товар?']")
    WebElement whenButton;
    @FindBy(xpath = "//button[@class='sp-tablinks' and .='Скільки коштує доставка?']")
    WebElement priceButton;
    @FindBy(xpath = "//button[@class='sp-tablinks' and .='Якими способами можна оплатити товар?']")
    WebElement payButton;
    @FindBy(xpath = "//button[@class='sp-tablinks' and .='Перевірка відправлень']")
    WebElement checkButton;
    @FindBy(xpath = "//h3[@class='sub-block-header' and .='Як отримати товар?']")
    WebElement howTOGetProduct;
    @FindBy(xpath = "//h4[contains(text(),'Самовивіз з магазину АЛЛО (безкоштовно)')]")
    WebElement pickupFromTheStore;
    @FindBy(xpath = "//h4[contains(text(),'Доставка в квартиру АЛЛО EXPRESS')]")
    WebElement deliveryToTheApartment;
    @FindBy(xpath = "//h4[contains(text(),'Самовивіз з «Нова пошта»')]")
    WebElement pickupFromNovaPoshta;
    @FindBy(xpath = "//h4[contains(text(),'Самовивіз з «Meest ПОШТА»')]")
    WebElement pickupFromMeestPost;
    @FindBy(xpath = "//h4[contains(text(),'Доставка «Meest ПОШТА»')]")
    WebElement deliveryByMeestPost;
    @FindBy(xpath = "//h4[contains(text(),'Доставка «Нова пошта»')]")
    WebElement deliveryByNovaPoshta;
    @FindBy(xpath = "//img[@class='np__img' and @alt='nova poshta']")
    WebElement novaPoshtaImg;
    @FindBy(xpath = "//img[@class='meest__img' and @alt='meest']")
    WebElement meestImg;
    @FindBy(xpath = "//p[contains(.,'Доставка товарів по Україні в магазини АЛЛО здійснюється безкоштовно!')]")
    WebElement deliveryInfo;
    @FindBy(xpath = "//li[.='Безготівкова оплата']")
    WebElement paymentCashless;
    @FindBy(xpath = "//li[.='Готівкова оплата']")
    WebElement paymentCash;
    @FindBy(xpath = "//li[.='Платіжними картами Visa і MasterCard']")
    WebElement paymentCard;
    @FindBy(xpath = "//p[normalize-space() = 'Одержувач при отриманні Відправлення має підписати Експрес-накладну. Проставленням підпису на Експрес-накладній Одержувач підтверджує, що:']")
    WebElement recipientsRights;
    @FindBy(xpath = "//tr[@class='sp-proverka' and ./td[.='Мобільні телефони'] and ./td[.='3-5 хв.']]")
    WebElement examinationPhone;
    @FindBy(xpath = "//tr[@class='sp-proverka' and ./td[.='Ноутбуки'] and ./td[.='5-7 хв.']]")
    WebElement examinationLaptops;
    @FindBy(xpath = "//tr[@class='sp-proverka' and ./td[.='Телевізори'] and ./td[.='10 хв.']]")
    WebElement examinationTVs;
    @FindBy(xpath = "//tr[@class='sp-proverka' and ./td[.='Фото/відео техніка'] and ./td[.='3-7 хв.']]")
    WebElement examinationPhotoVideoEquipment;
    @FindBy(xpath = "//tr[@class='sp-proverka' and ./td[.='Кондиціонери, водонагрівачі та обігрівачі'] and ./td[.='2-3 хв.']]")
    WebElement examinationHVAC;
    @FindBy(xpath = "//tr[@class='sp-proverka' and ./td[.='Дрібна техніка'] and ./td[.='3-7 хв.']]")
    WebElement examinationSmallAppliances;
    @FindBy(xpath = "//p[@class='sp-paragraph' and .= 'При виникненні додаткових питань з приводу огляду відправлень – звертайтесь за номером гарячої лінії 0800300100 або в чат.']")
    WebElement additionalQuestionsParagraph;

    ShipmentPaymentPage() {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

}
