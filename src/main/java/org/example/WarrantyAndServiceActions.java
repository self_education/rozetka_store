package org.example;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class WarrantyAndServiceActions extends HeaderActions {

    WarrantyAndServicePage warrantyAndServicePage = new WarrantyAndServicePage();

    public WarrantyAndServiceActions verifyWarrantyOnItemsHeaderVisible() {
        waitVisible(warrantyAndServicePage.warrantyOnItemsHeader);
        return this;
    }

    public WarrantyAndServiceActions verifyWarrantyPrepareItemHeader() {
        waitVisible(warrantyAndServicePage.warrantyPrepareItemHeader);
        return this;
    }

    public WarrantyAndServiceActions verifyWarrantyPrepareDocsHeader() {
        waitVisible(warrantyAndServicePage.warrantyPrepareDocsHeader);
        return this;
    }

    public WarrantyAndServiceActions verifyWarrantySendItemToServiceHeader() {
        waitVisible(warrantyAndServicePage.warrantySendItemToServiceHeader);
        return this;
    }

    public WarrantyAndServiceActions verifyWarrantyHowToSendItemToServiceHeader() {
        waitVisible(warrantyAndServicePage.warrantyHowToSendItemToServiceHeader);
        return this;
    }

    public WarrantyAndServiceActions clickExchangeAndReturnButton() {
        warrantyAndServicePage.exchangeAndReturnButton.click();
        return this;
    }

    public WarrantyAndServiceActions verifyExchangeAndReturnHeader() {
        waitVisible(warrantyAndServicePage.exchangeAndReturnHeader);
        return this;
    }

    public WarrantyAndServiceActions verifyReturnUnsatisfactoryProductHeader() {
        waitVisible(warrantyAndServicePage.returnUnsatisfactoryProductHeader);
        return this;
    }

    public WarrantyAndServiceActions verifyReturnPrepareDocsHeader() {
        waitVisible(warrantyAndServicePage.returnPrepareDocsHeader);
        return this;
    }

    public WarrantyAndServiceActions verifyReturnIfNOTOutOfTimeHeaderHeader() {
        waitVisible(warrantyAndServicePage.returnIfNOTOutOfTimeHeader);
        return this;
    }

    public WarrantyAndServiceActions clickServiceCentreActButton(){
        warrantyAndServicePage.serviceCentreActButton.click();
        return this;
    }

    public WarrantyAndServiceActions verifyServiceCentreActHeader(){
        waitVisible(warrantyAndServicePage.serviceCentreActHeader);
        return this;
    }

    public WarrantyAndServiceActions clickServiceCentresButton(){
        warrantyAndServicePage.serviceCentresButton.click();
        return this;
    }

    public WarrantyAndServiceActions verifyServiceCentresAddressesHeader(){
        waitVisible(warrantyAndServicePage.serviceCentresAddressesHeader);
        return this;
    }

    public WarrantyAndServiceActions clickCheckRepairStatusButton(){
        warrantyAndServicePage.checkRepairStatusButton.click();
        return this;
    }

    public WarrantyAndServiceActions verifyCheckRepairRequestStatusHeader(){
        waitVisible(warrantyAndServicePage.checkRepairRequestStatusHeader);
        return this;
    }

    public WarrantyAndServiceActions clickCheckRepairRequestStatusButton(){
        warrantyAndServicePage.checkRepairRequestStatusButton.click();
        return this;
    }

    public WarrantyAndServiceActions verifyCheckRepairRequestStatusPopUpHeader(){
        waitVisible(warrantyAndServicePage.checkRepairRequestStatusPopUpHeader);
        return this;
    }

    public WarrantyAndServiceActions verifyCheckRepairRequestStatusPopUpDescription(){
        waitVisible(warrantyAndServicePage.checkRepairRequestStatusPopUpDescription);

        softAssert.assertTrue(WarrantyAndServiceDataBase.RETURN_CHECK_REPAIR_REQUEST_STATUS_POP_UP_DESCRIPTION.getMessage()
                .equals(warrantyAndServicePage.checkRepairRequestStatusPopUpDescription.getText()), "\n Wrong Description" +
                "\n Current title is: " + warrantyAndServicePage.checkRepairRequestStatusPopUpDescription.getText() +
                "\n Expected result: " + WarrantyAndServiceDataBase.RETURN_CHECK_REPAIR_REQUEST_STATUS_POP_UP_DESCRIPTION.getMessage() + "\n");
        softAssert.assertAll();
        return this;
    }

    public WarrantyAndServiceActions inputCheckRepairRequestStatusPopUpID(String id){
        warrantyAndServicePage.checkRepairRequestStatusPopUpInput.click();
        warrantyAndServicePage.checkRepairRequestStatusPopUpInput.sendKeys(id);
        return this;
    }

    public WarrantyAndServiceActions clickCheckTheStatusButton(){
        warrantyAndServicePage.checkTheStatusButton.click();
        return this;
    }

    public WarrantyAndServiceActions verifyCheckRepairRequestNotFoundPopUpTextExists(){
        waitVisible(warrantyAndServicePage.checkRepairRequestStatusPopUpInfo);

        softAssert.assertTrue(WarrantyAndServiceDataBase.RETURN_CHECK_REPAIR_REQUEST_NOT_FOUND_POP_UP_TEXT.getMessage()
                .equals(warrantyAndServicePage.checkRepairRequestStatusPopUpInfo.getText()), "\n Wrong Request Respond" +
                "\n Current title is: " + warrantyAndServicePage.checkRepairRequestStatusPopUpInfo.getText() +
                "\n Expected result: " + WarrantyAndServiceDataBase.RETURN_CHECK_REPAIR_REQUEST_NOT_FOUND_POP_UP_TEXT.getMessage() + "\n");
        softAssert.assertAll();
        return this;
    }

    public WarrantyAndServiceActions verifyCheckRepairRequestValidationTextExists(){
        waitVisible(warrantyAndServicePage.checkRepairRequestValidationText);

        softAssert.assertTrue(WarrantyAndServiceDataBase.RETURN_CHECK_REPAIR_REQUEST_VALIDATION_TEXT.getMessage()
                .equals(warrantyAndServicePage.checkRepairRequestValidationText.getText()), "\n Wrong Request Respond" +
                "\n Current title is: " + warrantyAndServicePage.checkRepairRequestValidationText.getText() +
                "\n Expected result: " + WarrantyAndServiceDataBase.RETURN_CHECK_REPAIR_REQUEST_VALIDATION_TEXT.getMessage() + "\n");
        softAssert.assertAll();
        return this;
    }

    public WarrantyAndServiceActions verifyCheckRepairRequestNumbersOnlyTextExists(){
        waitVisible(warrantyAndServicePage.checkRepairRequestValidationText);

        softAssert.assertTrue(WarrantyAndServiceDataBase.RETURN_CHECK_REPAIR_REQUEST_NUMBERS_ONLY_TEXT.getMessage()
                .equals(warrantyAndServicePage.checkRepairRequestValidationText.getText()), "\n Wrong Request Respond" +
                "\n Current title is: " + warrantyAndServicePage.checkRepairRequestValidationText.getText() +
                "\n Expected result: " + WarrantyAndServiceDataBase.RETURN_CHECK_REPAIR_REQUEST_NUMBERS_ONLY_TEXT.getMessage() + "\n");
        softAssert.assertAll();
        return this;
    }

    public WarrantyAndServiceActions verifyCheckRepairRequestContactInfoMessageExists(){
        waitVisible(warrantyAndServicePage.checkRepairRequestContactInfoMessage);

        softAssert.assertTrue(WarrantyAndServiceDataBase.RETURN_CHECK_REPAIR_REQUEST_CONTACT_INFO_MESSAGE.getMessage()
                .equals(warrantyAndServicePage.checkRepairRequestContactInfoMessage.getText()), "\n Wrong Request Respond" +
                "\n Current title is: " + warrantyAndServicePage.checkRepairRequestContactInfoMessage.getText() +
                "\n Expected result: " + WarrantyAndServiceDataBase.RETURN_CHECK_REPAIR_REQUEST_CONTACT_INFO_MESSAGE.getMessage() + "\n");
        softAssert.assertAll();
        return this;
    }

    public WarrantyAndServiceActions clickCheckRepairRequestCloseButton(){
        warrantyAndServicePage.checkRepairRequestCloseButton.click();
        return this;
    }

    public WarrantyAndServiceActions clickTheFAQsButton(){
        warrantyAndServicePage.theFAQs.click();
        return this;
    }

    public WarrantyAndServiceActions clickTheFAQsTotFitButton(){
        warrantyAndServicePage.theFAQsNotFit.click();
        return this;
    }

    public WarrantyAndServiceActions verifyTheFAQsNotFitButtonTextCorrect(){

        softAssert.assertTrue(WarrantyAndServiceDataBase.RETURN_CHECK_RETURN_FAQS_NOT_FIT.getMessage()
                .equals(warrantyAndServicePage.theFAQsNotFit.getText()), "\n Wrong Header" +
                "\n Current title is: " + warrantyAndServicePage.theFAQsNotFit.getText() +
                "\n Expected result: " + WarrantyAndServiceDataBase.RETURN_CHECK_RETURN_FAQS_NOT_FIT.getMessage() + "\n");
        softAssert.assertAll();
        return this;
    }

    public WarrantyAndServiceActions verifyTheFAQsCannotBeReturned(){

        softAssert.assertTrue(WarrantyAndServiceDataBase.RETURN_CHECK_RETURN_FAQS_CANNOT_BE_RETURNED.getMessage()
                .equals(warrantyAndServicePage.theFAQsCannotBeReturned.getText()), "\n Wrong Header" +
                "\n Current title is: " + warrantyAndServicePage.theFAQsCannotBeReturned.getText() +
                "\n Expected result: " + WarrantyAndServiceDataBase.RETURN_CHECK_RETURN_FAQS_CANNOT_BE_RETURNED.getMessage() + "\n");
        softAssert.assertAll();
        return this;
    }

    public WarrantyAndServiceActions verifyTheFAQsCheckLost(){

        softAssert.assertTrue(WarrantyAndServiceDataBase.RETURN_CHECK_RETURN_FAQS_CHECK_LOST.getMessage()
                .equals(warrantyAndServicePage.theFAQsCheckLost.getText()), "\n Wrong Header" +
                "\n Current title is: " + warrantyAndServicePage.theFAQsCheckLost.getText() +
                "\n Expected result: " + WarrantyAndServiceDataBase.RETURN_CHECK_RETURN_FAQS_CHECK_LOST.getMessage() + "\n");
        softAssert.assertAll();
        return this;
    }

    public WarrantyAndServiceActions verifyTheFAQsWarrantyPeriod(){

        softAssert.assertTrue(WarrantyAndServiceDataBase.RETURN_CHECK_RETURN_FAQS_WARRANTY_PERIOD.getMessage()
                .equals(warrantyAndServicePage.theFAQsWarrantyPeriod.getText()), "\n Wrong Header" +
                "\n Current title is: " + warrantyAndServicePage.theFAQsWarrantyPeriod.getText() +
                "\n Expected result: " + WarrantyAndServiceDataBase.RETURN_CHECK_RETURN_FAQS_WARRANTY_PERIOD.getMessage() + "\n");
        softAssert.assertAll();
        return this;
    }

    public WarrantyAndServiceActions verifyTheFAQsBreaksDownDuringTheWarrantyPeriod(){

        softAssert.assertTrue(WarrantyAndServiceDataBase.RETURN_CHECK_RETURN_FAQS_BREAKS_DOWN_DURING_THE_WARRANTY_PERIOD.getMessage()
                .equals(warrantyAndServicePage.theFAQsBreaksDownDuringTheWarrantyPeriod.getText()), "\n Wrong Header" +
                "\n Current title is: " + warrantyAndServicePage.theFAQsBreaksDownDuringTheWarrantyPeriod.getText() +
                "\n Expected result: " + WarrantyAndServiceDataBase.RETURN_CHECK_RETURN_FAQS_BREAKS_DOWN_DURING_THE_WARRANTY_PERIOD.getMessage() + "\n");
        softAssert.assertAll();
        return this;
    }

    public WarrantyAndServiceActions verifyTheFAQsVerifyXiaomiIMEI(){

        softAssert.assertTrue(WarrantyAndServiceDataBase.RETURN_CHECK_RETURN_FAQS_VERIFY_XIAOMI_IMEI.getMessage()
                .equals(warrantyAndServicePage.theFAQsVerifyXiaomiIMEI.getText()), "\n Wrong Header" +
                "\n Current title is: " + warrantyAndServicePage.theFAQsVerifyXiaomiIMEI.getText() +
                "\n Expected result: " + WarrantyAndServiceDataBase.RETURN_CHECK_RETURN_FAQS_VERIFY_XIAOMI_IMEI.getMessage() + "\n");
        softAssert.assertAll();
        return this;
    }

    public WarrantyAndServiceActions clickReviewsButton(){
        warrantyAndServicePage.reviewsButton.click();
        return this;
    }
    
    public WarrantyAndServiceActions verifyReviewsHeaderExists(){
        warrantyAndServicePage.reviewsHeader.isDisplayed();
        return this;
    }

    public WarrantyAndServiceActions verifyReviewsQuestionOneHeader(){

        softAssert.assertTrue(WarrantyAndServiceDataBase.REVIEWS_QUESTION_ONE_HEADER.getMessage()
                .equals(warrantyAndServicePage.reviewsQuestionOneHeader.getText()), "\n Wrong Header" +
                "\n Current title is: " + warrantyAndServicePage.reviewsQuestionOneHeader.getText() +
                "\n Expected result: " + WarrantyAndServiceDataBase.REVIEWS_QUESTION_ONE_HEADER.getMessage() + "\n");
        softAssert.assertAll();
        return this;
    }

    public WarrantyAndServiceActions verifyReviewsQuestionTwoHeader(){

        softAssert.assertTrue(WarrantyAndServiceDataBase.REVIEWS_QUESTION_TWO_HEADER.getMessage()
                .equals(warrantyAndServicePage.reviewsQuestionTwoHeader.getText()), "\n Wrong Header" +
                "\n Current title is: " + warrantyAndServicePage.reviewsQuestionTwoHeader.getText() +
                "\n Expected result: " + WarrantyAndServiceDataBase.REVIEWS_QUESTION_TWO_HEADER.getMessage() + "\n");
        softAssert.assertAll();
        return this;
    }

    public WarrantyAndServiceActions verifyReviewsQuestionThreeHeader(){

        softAssert.assertTrue(WarrantyAndServiceDataBase.REVIEWS_QUESTION_THREE_HEADER.getMessage()
                .equals(warrantyAndServicePage.reviewsQuestionThreeHeader.getText()), "\n Wrong Header" +
                "\n Current title is: " + warrantyAndServicePage.reviewsQuestionThreeHeader.getText() +
                "\n Expected result: " + WarrantyAndServiceDataBase.REVIEWS_QUESTION_THREE_HEADER.getMessage() + "\n");
        softAssert.assertAll();
        return this;
    }

    public WarrantyAndServiceActions verifyReviewsQuestionFourHeader(){

        softAssert.assertTrue(WarrantyAndServiceDataBase.REVIEWS_QUESTION_FOUR_HEADER.getMessage()
                .equals(warrantyAndServicePage.reviewsQuestionFourHeader.getText()), "\n Wrong Header" +
                "\n Current title is: " + warrantyAndServicePage.reviewsQuestionFourHeader.getText() +
                "\n Expected result: " + WarrantyAndServiceDataBase.REVIEWS_QUESTION_FOUR_HEADER.getMessage() + "\n");
        softAssert.assertAll();
        return this;
    }

    public WarrantyAndServiceActions verifyReviewsQuestionLastStepHeaderExists(){
        warrantyAndServicePage.reviewsQuestionLastStepHeader.isEnabled();
        return this;
    }

    public WarrantyAndServiceActions clickFormReviewsNumberNineRadioButton(int reviewsValue, int questionNumber){
        warrantyAndServicePage.formReviewsNumberRadio(reviewsValue, questionNumber).click();
        new WebDriverWait(BeforeRun.getDriver(), 1).withTimeout(Duration.ofSeconds(2));
        return this;
    }

    public WarrantyAndServiceActions verifyFormReviewsNumberBackgroundColorIsCorrect(int reviewsValue, int questionNumber, String color){
        softAssert.assertTrue(
                color.equals(warrantyAndServicePage.formReviewsNumberSpan(reviewsValue, questionNumber).getCssValue("background-color")), "\n Wrong Color" +
                "\n Item's color is: " + warrantyAndServicePage.formReviewsNumberSpan(reviewsValue, questionNumber).getCssValue("background-color") +
                "\n Expected color: " + WarrantyAndServiceDataBase.NUMBER_BACKGROUND_IF_ACTIVE_COLOR.getMessage() + "\n");
        softAssert.assertAll();
        return this;
    }

}
