package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class AudioPage extends HeaderPage {

    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Навушники']")
    WebElement headphones;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Безпровідні навушники']")
    WebElement wirelessHeadphones;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Дротові навушники']")
    WebElement wiredHeadphones;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Портативна акустика']")
    WebElement portableSpeakers;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Колонки для ПК']")
    WebElement speakersForPCs;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Аксесуари для навушників']")
    WebElement headphoneAccessories;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Акустичні системи']")
    WebElement acousticSystems;

    public AudioPage() {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

}
