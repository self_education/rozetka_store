package org.example;

public enum UrlDataBase {

    PLUMBING_PAGE("https://allo.ua/ua/santehnika/"),
    REPAIR_PAGE("https://allo.ua/ua/remont-bytovoj/"),
    ELECTRICAL_INSTALLATION_EQUIPMENT_PAGE("https://allo.ua/ua/jelektromontazhnoe-oborudovanie/"),

    SMARTPHONE_PAGE("https://allo.ua/ua/products/mobile/klass-kommunikator_smartfon/"),
    PHONES_PAGE("https://allo.ua/ua/products/mobile/"),
    PHONE_FOR_HOME_PAGE("https://allo.ua/ua/telefony-dlja-doma-i-ofisa/"),
    PHONE_ACCESSORIES_PAGE("https://allo.ua/ua/aksessuary-k-mobilnym-telefonam/"),
    PHONE_SPARE_PARTS_PAGE("https://allo.ua/ua/zapchasti-dlja-telefonov/"),

    TELEVISIONS_PAGE("https://allo.ua/ua/televizory/"),
    DIGITAL_PHOTO_FRAMES_PAGE("https://allo.ua/ua/cifrovie-fotoramki/"),
    PROJECTION_EQUIPMENT_PAGE("https://allo.ua/ua/proekcionnoe-oborudovanie/"),
    PORTABLE_TVS_PAGE("https://allo.ua/ua/portativnye-televizory/"),
    PHOTO_AND_VIDEO_EQUIPMENT_PAGE("https://allo.ua/ua/aksessuary-dlya-foto-i-video/"),
    PLAYERS_MP3_PAGE("https://allo.ua/ua/products/62/"),

    CLIMATE_CONTROL_EQUIPMENT_PAGE("https://allo.ua/ua/klimaticheskaya-tehnika/"),
    HOME_APPLIANCES_PAGE("https://allo.ua/ua/tehnika-dlya-doma/"),
    COFFEE_WORLD_PAGE("https://allo.ua/ua/mir-kofe/"),
    KITCHEN_APPLIANCES_PAGE("https://allo.ua/ua/tehnika-dlya-kuhni/"),
    PERSONALISED_CARE_PAGE("https://allo.ua/ua/dlya-individualnogo-uhoda/"),
    LARGE_HOUSEHOLD_APPLIANCES_PAGE("https://allo.ua/ua/krupnobytovaja-tehnika/"),
    BUILT_IN_APPLIANCES_PAGE("https://allo.ua/ua/vstraivaemaja-tehnika/"),
    ACCESSORIES_PAGE("https://allo.ua/ua/aksessuary-k-bytovoj-tehnike/"),
    WATER_FILTERS_PAGE("https://allo.ua/ua/sistemy-ochistki-vody/"),
    SPARE_PARTS_FOR_HOUSEHOLD_APPLIANCES_PAGE("https://allo.ua/ua/zapchasti/"),

    TABLETS_PAGE("https://allo.ua/ua/products/internet-planshety/"),
    LAPTOPS_PAGE("https://allo.ua/ua/products/notebooks/"),
    NETWORK_EQUIPMENT_PAGE("https://allo.ua/ua/setevoe-oborudovanie/"),
    MONITORS_PAGE("https://allo.ua/ua/monitory/"),
    PERSONAL_COMPUTERS_PAGE("https://allo.ua/ua/kompjutery/"),
    MONO_BLOCKS_PAGE("https://allo.ua/ua/products/monobloki/"),
    NET_TOPS_PAGE("https://allo.ua/ua/nettopy/"),
    ACCESSORIES_FOR_TABLETS_PAGE("https://allo.ua/ua/aksessuary-dlja-internet-planshetov/"),
    ACCESSORIES_FOR_LAPTOPS_PAGE("https://allo.ua/ua/aksessuary-k-kompyuteram-i-noutbukam/"),
    PERIPHERALS_PAGE("https://allo.ua/ua/periferija-dlja-pk/"),
    E_BOOKS_PAGE("https://allo.ua/ua/elektronnye-knigi/"),
    GRAPHIC_TABLETS_PAGE("https://allo.ua/ua/graficheskie-planshety/"),
    ACCESSORIES_FOR_GRAPHIC_TABLETS_PAGE("https://allo.ua/ua/aksessuary-dlja-graficheskih-planshetov/"),
    LAPTOP_PARTS_PAGE("https://allo.ua/ua/komplektujuschie-dlja-noutbukov/"),
    PERSONAL_COMPUTER_PARTS_PAGE("https://allo.ua/ua/komplektujuschie/"),
    ACCESSORIES_FOR_PC_CASES_PAGE("https://allo.ua/ua/aksessuary-dlja-korpusov-pk/"),
    SERVERS_PAGE("https://allo.ua/ua/servery/"),

    PRINT_3D_PAGE("https://allo.ua/ua/3d-pechat/"),
    FITNESS_BRACELETS_PAGE("https://allo.ua/ua/fitnes-braslety/"),
    ACCESSORIES_FOR_SMARTWATCHES_PAGE("https://allo.ua/ua/aksessuary-k-smart-chasam/"),
    ELECTRIC_TRANSPORT_PAGE("https://allo.ua/ua/monocikly-i-segvei/"),
    GAME_ZONE_PAGE("https://allo.ua/ua/igrovaja-zona/"),
    ACTION_CAMERAS_AND_ACCESSORIES_PAGE("https://allo.ua/ua/jekshn-kamery-i-aksessuary/"),
    TOBACCO_HEATING_SYSTEMS_PAGE("https://allo.ua/ua/sistemy-nagrevanija-tabaka/"),
    SMART_KEY_RINGS_PAGE("https://allo.ua/ua/smart-brelki/"),
    SMART_RINGS_PAGE("https://allo.ua/ua/smart-kol-ca/"),

    HEADPHONES_PAGE("https://allo.ua/ua/naushniki/"),
    WIRELESS_HEADPHONES_PAGE("https://allo.ua/ua/naushniki/tip_podkljuchenija_naushniki-besprovodnoe/"),
    WIRED_HEADPHONES_PAGE("https://allo.ua/ua/naushniki/tip_podkljuchenija_naushniki-provodnoe/"),
    PORTABLE_SPEAKERS_PAGE("https://allo.ua/ua/portativnaja-akustika/"),
    SPEAKERS_FOR_PCS_PAGE("https://allo.ua/ua/kolonki-dlja-pk/"),
    HEADPHONE_ACCESSORIES_PAGE("https://allo.ua/ua/aksessuary-dlja-naushnikov/"),
    ACOUSTIC_SYSTEMS_PAGE("https://allo.ua/ua/akusticheskie-sistemy-i-kino-doma/"),

    BICYCLES_AND_ACCESSORIES_PAGE("https://allo.ua/ua/velosipedy-i-aksessuary/"),
    SKATEBOARDS_PAGE("https://allo.ua/ua/skejtbordy/"),
    DECKS_FOR_SKATEBOARDS_PAGE("https://allo.ua/ua/deki-dlja-skejtbordov/"),
    WHEELS_FOR_SKATEBOARDS_PAGE("https://allo.ua/ua/kolesa-dlja-skejtbordov/"),
    SPARE_PARTS_FOR_SKATEBOARDS_PAGE("https://allo.ua/ua/komplektujuschie-dlja-skejtbordov/"),
    SLIMMING_CLOTHES_PAGE("https://allo.ua/ua/odezhda-dlja-pohudenija/"),
    SIMULATORS_PAGE("https://allo.ua/ua/trenazhery/"),
    SPORTS_EQUIPMENT_PAGE("https://allo.ua/ua/sportivnyj-inventar/"),
    MARTIAL_ARTS_PAGE("https://allo.ua/ua/edinoborstva/"),
    DARTS_PAGE("https://allo.ua/ua/darts/"),
    TENNIS_AND_SQUASH_PAGE("https://allo.ua/ua/nastol-nyj-tennis-i-skvosh/"),
    ROLLERS_AND_PROTECTION_PAGE("https://allo.ua/ua/roliki-i-zaschita/"),
    WINTER_SPORTS_PAGE("https://allo.ua/ua/zimnij-sport/"),
    WATER_SPORTS_PAGE("https://allo.ua/ua/vodnyj-sport/"),
    HEALTH_AND_CARE_PAGE("https://allo.ua/ua/medtehnika/"),
    HEARING_AIDS_PAGE("https://allo.ua/ua/sluhovye-apparaty/"),
    CARDIAC_SENSORS_PAGE("https://allo.ua/ua/pul-somery/"),
    WALKIE_TALKIES_PAGE("https://allo.ua/ua/products/radiostancii/"),
    ACCESSORIES_FOR_WALKIE_TALKIES_PAGE("https://allo.ua/ua/aksessuary-k-racijam/"),
    BASKETBALL_PAGE("https://allo.ua/ua/basketbol/"),
    BILLIARDS_PAGE("https://allo.ua/ua/bil-jard/"),
    MINI_GOLF_PAGE("https://allo.ua/ua/mini-gol-f/"),
    PROTECTIVE_EQUIPMENT_PAGE("https://allo.ua/ua/nabor-zaschity/"),
    TRAMPOLINES_AND_ACCESSORIES_PAGE("https://allo.ua/ua/batuty-i-aksessuary/"),
    GYMNASTICS_PAGE("https://allo.ua/ua/gimnastika/"),
    HOCKEY_PAGE("https://allo.ua/ua/hokkej/"),
    FIGURE_SKATING_PAGE("https://allo.ua/ua/figurnoe-katanie/"),
    SPORTS_CUPS_PAGE("https://allo.ua/ua/sportivnye-kubki/"),
    SPORTS_MEDALS_PAGE("https://allo.ua/ua/sportivnye-medali/"),


    TENTS_PAGE("https://allo.ua/ua/palatki/"),
    LANTERNS_PAGE("https://allo.ua/ua/fonari/"),
    THERMAL_PRODUCTS_PAGE("https://allo.ua/ua/termoprodukcija/"),
    BACKPACKS_PAGE("https://allo.ua/ua/rjukzaki-i-sumki/"),
    BOATS_PAGE("https://allo.ua/ua/lodki/"),
    MOTORS_FOR_BOATS_PAGE("https://allo.ua/ua/lodochnye-motory/");
    private final String url;

    UrlDataBase(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
