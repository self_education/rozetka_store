package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class WarrantyAndServicePage extends HeaderPage {

    @FindBy(xpath = "//h5[@class='sub-block-header sp-h5-header ' and .= 'На всі товари, придбані в АЛЛО, надається гарантія, яка дає право на:']")
    WebElement warrantyOnItemsHeader;
    @FindBy(xpath = "//h5[@class='sub-block-header sp-h5-header ' and .= 'Підготуйте товар до гарантійного обслуговування']")
    WebElement warrantyPrepareItemHeader;
    @FindBy(xpath = "//h5[@class='sub-block-header sp-h5-header ' and .= 'Підготуйте, будь ласка, всі потрібні документи:']")
    WebElement warrantyPrepareDocsHeader;
    @FindBy(xpath = "//h5[@class='sub-block-header sp-h5-header ' and normalize-space()= 'Передайте товар на гарантійне обслуговування, обов’язково додайте зазначені вище документи']")
    WebElement warrantySendItemToServiceHeader;
    @FindBy(xpath = "//h6[@class = 'least-block-header sp-h6-header' and normalize-space() = 'Якщо ви придбали товар в одному з магазинів АЛЛО, виберіть будь-який зручний для вас спосіб передачі:']")
    WebElement warrantyHowToSendItemToServiceHeader;
    @FindBy(xpath = "//button[@class = 'sp-tablinks' and contains(.,'Обмін і повернення')]")
    WebElement exchangeAndReturnButton;
    @FindBy(xpath = "//h5[@class = 'sub-block-header sp-h5-header ' and .= 'Обмін і повернення по гарантії']")
    WebElement exchangeAndReturnHeader;
    @FindBy(xpath = "//h5[@class = 'sub-block-header sp-h5-header ' and normalize-space() = 'Як обміняти або повернути товар, який не влаштував, не сподобався або з іншої причини?']")
    WebElement returnUnsatisfactoryProductHeader;
    @FindBy(xpath = "//h5[@class = 'sub-block-header sp-h5-header ' and normalize-space() = 'Підготуйте, будь ласка, всі необхідні документи:']")
    WebElement returnPrepareDocsHeader;
    @FindBy(xpath = "//h5[@class = 'sub-block-header sp-h5-header ' and contains( ., 'Обміняйте або поверніть товар, якщо від дати купівлі не минуло 14')]")
    WebElement returnIfNOTOutOfTimeHeader;
    @FindBy(xpath = "//button[@class = 'sp-tablinks' and .= 'Акт сервісного центру']")
    WebElement serviceCentreActButton;
    @FindBy(xpath = "//h5[@class = 'sub-block-header sp-h5-header ' and contains( ., 'Акт сервісного центру')]")
    WebElement serviceCentreActHeader;
    @FindBy(xpath = "//button[@class = 'sp-tablinks' and .= 'Сервісні центри']")
    WebElement serviceCentresButton;
    @FindBy(xpath = "//h5[@class = 'sub-block-header sp-h5-header ' and contains( ., 'Адреси сервісних центрів виробників')]")
    WebElement serviceCentresAddressesHeader;
    @FindBy(xpath = "//button[@class = 'sp-tablinks' and .= 'Перевірка статусу ремонту']")
    WebElement checkRepairStatusButton;
    @FindBy(xpath = "//h5[@class = 'sub-block-header sp-h5-header ' and contains( ., 'Перевірка статусу заявки на ремонт')]")
    WebElement checkRepairRequestStatusHeader;
    @FindBy(xpath = "//button[@class = 'warranty-service-widget' and normalize-space() = 'Перевірка статусу заявки на ремонт']")
    WebElement checkRepairRequestStatusButton;
    @FindBy(xpath = "//p[@class = 'ws-popup__title' and .= 'Перевірка статусу заявки на ремонт']")
    WebElement checkRepairRequestStatusPopUpHeader;
    @FindBy(xpath = "//p/following-sibling::p[@class = 'ws-popup__description']")
    WebElement checkRepairRequestStatusPopUpDescription;
    @FindBy(xpath = "//input[@id='ws-popup__control-input']")
    WebElement checkRepairRequestStatusPopUpInput;
    @FindBy(xpath = "//button[@class='ws-popup__control-submit']")
    WebElement checkTheStatusButton;
    @FindBy(xpath = "//p[@class='ws-popup__message-text']")
    WebElement checkRepairRequestStatusPopUpInfo;
    @FindBy(xpath = "//span[@class='validation-text']")
    WebElement checkRepairRequestValidationText;
    @FindBy(xpath = "//div[@class = 'ws-popup__footer']")
    WebElement checkRepairRequestContactInfoMessage;
    @FindBy(xpath = "//div[@class = 'v-modal__close-btn']")
    WebElement checkRepairRequestCloseButton;
    @FindBy(xpath = "//button[@class = 'sp-tablinks' and .= 'Запитання та відповіді']")
    WebElement theFAQs;
    @FindBy(xpath = "//label[@for='war3']")
    WebElement theFAQsNotFit;
    @FindBy(xpath = "//label[@for='war4']")
    WebElement theFAQsCannotBeReturned;
    @FindBy(xpath = "//label[@for='war5']")
    WebElement theFAQsCheckLost;
    @FindBy(xpath = "//label[@for='war6']")
    WebElement theFAQsWarrantyPeriod;
    @FindBy(xpath = "//label[@for='war8']")
    WebElement theFAQsBreaksDownDuringTheWarrantyPeriod;
    @FindBy(xpath = "//label[@for='war9']")
    WebElement theFAQsVerifyXiaomiIMEI;
    @FindBy(xpath = "//button[@class = 'sp-tablinks' and .= 'Відгуки']")
    WebElement reviewsButton;
    @FindBy(xpath = "//div[@id='Reviews']//h5[.='Шановний клієнт']")
    WebElement reviewsHeader;
    @FindBy(xpath = "//h5[@class='sub-block-header sp-h5-header'][1]")
    WebElement reviewsQuestionOneHeader;
    @FindBy(xpath = "//h5[@class='sub-block-header sp-h5-header'][2]")
    WebElement reviewsQuestionTwoHeader;
    @FindBy(xpath = "//h5[@class='sub-block-header sp-h5-header'][3]")
    WebElement reviewsQuestionThreeHeader;
    @FindBy(xpath = "//h5[@class='sub-block-header sp-h5-header'][4]")
    WebElement reviewsQuestionFourHeader;
    @FindBy(xpath = "//h5[@class='sub-block-header sp-h5-header' and .= 'Останній крок']")
    WebElement reviewsQuestionLastStepHeader;
    WarrantyAndServicePage() {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

    public WebElement formReviewsNumberRadio(int reviewsValue, int questionNumber) {
        String formReviewsNumberRadio = "(//div/label[./input[@type='radio' and @value='" + reviewsValue + "']])[" + questionNumber + "]";
        return driver.findElement(By.xpath(formReviewsNumberRadio));
    }
    public WebElement formReviewsNumberSpan(int reviewsValue, int questionNumber) {
        String formReviewsNumberSpan = "(//input[@type='radio' and @value='" + reviewsValue + "']/following-sibling::span)[" + questionNumber + "]";
        return driver.findElement(By.xpath(formReviewsNumberSpan));
    }
}
