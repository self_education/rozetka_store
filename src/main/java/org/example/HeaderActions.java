package org.example;

import org.openqa.selenium.By;

import static org.example.BeforeRun.getDriver;

import org.openqa.selenium.interactions.Actions;

public class HeaderActions extends BaseActions {
    HeaderPage header = new HeaderPage();
    Actions action = new Actions(getDriver());

    public HeaderActions verifyHeaderUrl(String url) {
        UrlVerifier.UrlVerifier(url);
        return this;
    }

    public TradeInActions clickTradeIn(){
        header.tradeIn.click();
        return new TradeInActions();
    }

    public HeaderActions clickCatalogMenu() {
        waitClickable(header.catalogMenu);
        header.catalogMenu.click();
        return this;
    }

    public TopSalesActions clickTopSales() {
        waitClickable(header.holdCharge);
        action.moveToElement(header.holdCharge).perform();
        header.topSales.click();
        return new TopSalesActions();
    }

    public PhonesActions clickPhones() {
        header.phones.click();
        return new PhonesActions();
    }

    public TelevisionsAndMultimediaActions clickTelevisionsAndMultimedia() {
        waitClickable(header.televisionsAndMultimedia);
        header.televisionsAndMultimedia.click();
        return new TelevisionsAndMultimediaActions();
    }

    public HouseholdAppliancesActions clickHouseholdAppliances() {
        waitClickable(header.householdAppliances);
        header.householdAppliances.click();
        return new HouseholdAppliancesActions();
    }

    public LaptopsPCActions clickLaptopsPC() {
        waitClickable(header.laptopsPC);
        header.laptopsPC.click();
        return new LaptopsPCActions();
    }

    public AppleActions clickApple() {
        waitClickable(header.apple);
        header.apple.click();
        return new AppleActions();
    }

    public XiaomiActions clickXiaomi() {
        waitClickable(header.xiaomi);
        header.xiaomi.click();
        return new XiaomiActions();
    }

    public GadgetsAndElectricVehiclesActions clickGadgetsAndElectricVehicles() {
        waitClickable(header.gadgetsAndElectricVehicles);
        header.gadgetsAndElectricVehicles.click();
        return new GadgetsAndElectricVehiclesActions();
    }

    public AudioActions clickAudio() {
        waitClickable(header.audio);
        header.audio.click();
        return new AudioActions();
    }

    public SportAndHealthActions clickSportAndHealth() {
        waitClickable(header.sportAndHealth);
        header.sportAndHealth.click();
        return new SportAndHealthActions();
    }

    public TourismAndFishingActions clickTourismAndFishing() {
        waitClickable(header.tourismAndFishing);
        header.tourismAndFishing.click();
        return new TourismAndFishingActions();
    }

    public RepairAndEquipmentActions clickRepairAndEquipment() {
        waitClickable(header.repairAndEquipment);
        header.repairAndEquipment.click();
        return new RepairAndEquipmentActions();
    }

    public HeaderActions searchItem(String input) {
        header.searchItem.click();
        header.searchItem.sendKeys(input);
        return new HeaderActions();
    }

    public ShipmentPaymentActions clickShipmentPayment(){
        header.shipmentPayment.click();
        return new ShipmentPaymentActions();
    }

    public WarrantyAndServiceActions clickWarrantyAndService(){
        header.warrantyAndService.click();
        return new WarrantyAndServiceActions();
    }

    //    xiomi readmi 10//  for example
    public SearchActions clickUnExistedItemInSearch(String name) {
        waitVisible(header.item);
        getDriver().findElement(By.xpath("//li[@class = 'search-models__items']/a/span[contains(text(), '" + name + "')]")).click();
        return new SearchActions();
    }

    public ItemActions clickItemByNameInSearch(String name) {
        waitVisible(header.allResults);
        getDriver().findElement(By.xpath("//li[@class = 'search-products__items']/a/p[text() = '" + name + "']")).click();
        return new ItemActions();
    }

    public ItemActions clickItemByNameAndPriceInSearch(String name, String prise) {
        waitVisible(header.allResults);
        getDriver().findElement(By.xpath("//li[@class = 'search-products__items']//p[text() = '" + name + "' and following-sibling::div//div[@class = 'search-products__price-box--new-price']//span[text() = '" + prise + "']]")).click();
        return new ItemActions();
    }

    public SearchActions clickAllResults() {
        waitVisible(header.allResults);
        header.allResults.click();
        return new SearchActions();
    }

}
