package org.example;

public class LaptopsPCActions extends HeaderActions {

    LaptopsPCPage laptopsPCPage = new LaptopsPCPage();

    public LaptopsPCActions() {
        UrlVerifier.UrlVerifier("https://allo.ua/ua/planshety-i-gadzhety/");
    }
   
    public LaptopsPCActions clickTablets() {
        laptopsPCPage.tablets.click();
        return this;
    }
    
    public LaptopsPCActions clickLaptops() {
        laptopsPCPage.laptops.click();
        return this;
    }
    
    public LaptopsPCActions clickNetworkEquipment() {
        laptopsPCPage.networkEquipment.click();
        return this;
    }
    
    public LaptopsPCActions clickMonitors() {
        laptopsPCPage.Monitors.click();
        return this;
    }
    
    public LaptopsPCActions clickPersonalComputers() {
        laptopsPCPage.personalComputers.click();
        return this;
    }
    
    public LaptopsPCActions clickMonoBlocks() {
        laptopsPCPage.monoBlocks.click();
        return this;
    }
    
    public LaptopsPCActions clickNetTops() {
        laptopsPCPage.netTops.click();
        return this;
    }
    
    public LaptopsPCActions clickAccessoriesForTablets() {
        laptopsPCPage.accessoriesForTablets.click();
        return this;
    }
    
    public LaptopsPCActions clickAccessoriesForLaptops() {
        laptopsPCPage.accessoriesForLaptops.click();
        return this;
    }
    
    public LaptopsPCActions clickPeripherals() {
        laptopsPCPage.peripherals.click();
        return this;
    }
    
    public LaptopsPCActions clickE_Books() {
        laptopsPCPage.e_Books.click();
        return this;
    }
    
    public LaptopsPCActions clickGraphicTablets() {
        laptopsPCPage.graphicTablets.click();
        return this;
    }
    
    public LaptopsPCActions clickAccessoriesForGraphicTablets() {
        laptopsPCPage.accessoriesForGraphicTablets.click();
        return this;
    }
    
    public LaptopsPCActions clickLaptopParts() {
        laptopsPCPage.laptopParts.click();
        return this;
    }
    
    public LaptopsPCActions clickPersonalComputerParts() {
        laptopsPCPage.personalComputerParts.click();
        return this;
    }
    
    public LaptopsPCActions clickAccessoriesForPCCases() {
        laptopsPCPage.accessoriesForPCCases.click();
        return this;
    }
    
    public LaptopsPCActions clickServers() {
        laptopsPCPage.servers.click();
        return this;
    }

    @Override
    public LaptopsPCActions verifyHeaderUrl(String url) {
        UrlVerifier.UrlVerifier(url);
        return this;
    }

}
