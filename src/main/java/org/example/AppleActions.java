package org.example;

public class AppleActions extends HeaderActions {
    ApplePage applePage = new ApplePage();

    public AppleActions() {
        UrlVerifier.UrlVerifier("https://allo.ua/ua/apple-store/");
    }

    public AppleActions clickMac() {
        applePage.menuMac.click();
        return this;
    }
    public AppleActions clickMacBookAir15Img() {
        applePage.macBookAir15Img.click();
        return this;
    }

    public AppleActions clickIPad() {
        applePage.menuIPad.click();
        return this;
    }

    public AppleActions clickIPhone() {
        applePage.menuIPhone.click();
        return this;
    }

    public AppleActions clickWatch() {
        applePage.menuWatch.click();
        return this;
    }
    public AppleActions clickTV() {
        applePage.menuTV.click();
        return this;
    }
    public AppleActions clickAirTag() {
        applePage.menuAirTag.click();
        return this;
    }

    public AppleActions clickMusic() {
        applePage.menuMusic.click();
        return this;
    }

    public AppleActions verifyIPhoneDescriptionIsDisplayed() {
        softAssert.assertTrue(applePage.descriptionIPhone.isDisplayed(), "Item is not displayed.");
        return this;
    }

    public AppleActions verifyFooterIsDisplayed() {
        softAssert.assertTrue(applePage.footer.isDisplayed(), "Item is not displayed.");
        return this;
    }

    public AppleActions verifyMacDescriptionInVisibleArea() {
            verifyElementInVisibleArea(applePage.descriptionMac.getLocation().getY());
        return this;
    }

    public AppleActions verifyIPadDescriptionInVisibleArea() {
            verifyElementInVisibleArea(applePage.descriptionIPad.getLocation().getY());
        return this;
    }

    public AppleActions verifyIPhoneDescriptionInVisibleArea() {
            verifyElementInVisibleArea(applePage.descriptionIPhone.getLocation().getY());
        return this;
    }

    public AppleActions verifyWatchDescriptionInVisibleArea() {
            verifyElementInVisibleArea(applePage.descriptionWatch.getLocation().getY());
        return this;
    }

    public AppleActions verifyTVDescriptionInVisibleArea() {
            verifyElementInVisibleArea(applePage.descriptionTV.getLocation().getY());
        return this;
    }

    public AppleActions verifyAirTagDescriptionInVisibleArea() {
            verifyElementInVisibleArea(applePage.descriptionAirTag.getLocation().getY());
        return this;
    }

    public AppleActions verifyMusicDescriptionInVisibleArea() {
            verifyElementInVisibleArea(applePage.descriptionMusic.getLocation().getY());
        return this;
    }

//    public void assertAll() {
//        softAssert.assertAll();
//    }

}
