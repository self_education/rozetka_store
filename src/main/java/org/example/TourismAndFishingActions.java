package org.example;

public class TourismAndFishingActions extends HeaderActions {

    TourismAndFishingPage tourismAndFishingPage = new TourismAndFishingPage();

    public TourismAndFishingActions() {
        UrlVerifier.UrlVerifier("https://allo.ua/ua/turizm-i-rybalka/");
    }

    public TourismAndFishingActions clickTents() {
        tourismAndFishingPage.tents.click();
        return this;
    }

    public TourismAndFishingActions clickLanterns() {
        tourismAndFishingPage.lanterns.click();

        return this;
    }

    public TourismAndFishingActions clickThermalProducts() {
        tourismAndFishingPage.thermalProducts.click();
        return this;
    }

    public TourismAndFishingActions clickBackpacks() {
        tourismAndFishingPage.backpacks.click();
        return this;
    }

    public TourismAndFishingActions clickInflatableBoats() {
        tourismAndFishingPage.inflatableBoats.click();
        return this;
    }

    public TourismAndFishingActions clickMotorsForBoats() {
        tourismAndFishingPage.motorsForBoats.click();
        return this;
    }

    @Override
    public TourismAndFishingActions verifyHeaderUrl(String url) {
        UrlVerifier.UrlVerifier(url);
        return this;
    }

}
