package org.example;

public class TelevisionsActions extends HeaderActions implements Products, ElementExistence {

    TelevisionsPage televisionsPage = new TelevisionsPage();

    public TelevisionsActions() {
        UrlVerifier.UrlVerifier("https://allo.ua/ua/televizory/");
    }

    public TelevisionsActions clickLEDTelevisionType() {
        televisionsPage.ledTelevisionType.click();
        return this;
    }

    public TelevisionsActions clickQLEDTelevisionType() {
        televisionsPage.qledTelevisionType.click();
        return this;
    }

    public TelevisionsActions clickShowButton() {
        televisionsPage.showButton.click();
        return this;
    }

}
