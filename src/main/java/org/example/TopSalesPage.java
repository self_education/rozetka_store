package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class TopSalesPage extends HeaderPage {

    @FindBy(xpath = "//div[@class = 'accordion__header' and contains(text(), 'акції')]")
    public WebElement promoDetails;

    TopSalesPage(){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }
}
