package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;


public class PhonesPage extends HeaderPage {
@FindBy(xpath = "//a[@class = 'portal-category__item-link' and . = 'Смартфони']")
    WebElement smartphone;
@FindBy(xpath = "//a[@class = 'portal-category__item-link' and . = 'Смартфони і мобільні телефони']")
    WebElement phone;
@FindBy(xpath = "//a[@class = 'portal-category__item-link' and . = 'Телефони для дому та офісу']")
    WebElement phoneForHome;
@FindBy(xpath = "//a[@class = 'portal-category__item-link' and . = 'Аксесуари до смартфонів']")
    WebElement phoneAccessories;
@FindBy(xpath = "//a[@class = 'portal-category__item-link' and . = 'Запчастини для смартфонів']")
    WebElement phoneSpareParts;

PhonesPage(){
    PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
}

}

