package org.example;

public class AudioActions extends HeaderActions {

    AudioPage audioPage = new AudioPage();

    public AudioActions() {
        UrlVerifier.UrlVerifier("https://allo.ua/ua/naushniki-i-akustika/");
    }

    public AudioActions clickHeadphones() {
        audioPage.headphones.click();
        return this;
    }

    public AudioActions clickWirelessHeadphones() {
        audioPage.wirelessHeadphones.click();
        return this;
    }

    public AudioActions clickWiredHeadphones() {
        audioPage.wiredHeadphones.click();
        return this;
    }

    public AudioActions clickPortableSpeakers() {
        audioPage.portableSpeakers.click();
        return this;
    }

    public AudioActions clickSpeakersForPCs() {
        audioPage.speakersForPCs.click();
        return this;
    }

    public AudioActions clickHeadphoneAccessories() {
        audioPage.headphoneAccessories.click();
        return this;
    }

    public AudioActions clickAcousticSystems() {
        audioPage.acousticSystems.click();
        return this;
    }

    @Override
    public AudioActions verifyHeaderUrl(String url) {
        UrlVerifier.UrlVerifier(url);
        return this;
    }

}
