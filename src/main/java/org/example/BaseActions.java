package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import static org.example.BeforeRun.getDriver;

public abstract class BaseActions {
    SoftAssert softAssert = new SoftAssert();

    protected WebDriverWait wait = new WebDriverWait(getDriver(), 30);

    protected WebElement waitClickable(WebElement element) {
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    protected WebElement waitClickable(By element) {
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    protected WebElement waitVisible(WebElement element) {
        return wait.until(ExpectedConditions.visibilityOf(element));
    }

    protected WebElement waitAppear(By element) {
        return wait.until(ExpectedConditions.presenceOfElementLocated(element));
    }

    protected void verifyElementInVisibleArea(int elementTop) {
        // Get the current vertical scroll position
        int topPosition = ((Long) ((JavascriptExecutor) getDriver()).executeScript("return window.scrollY")).intValue();
        int bottomPosition = ((Long) ((JavascriptExecutor) getDriver()).executeScript("return window.scrollY + window.innerHeight")).intValue();

        // Check if the element's top position is within the current viewport
        softAssert.assertTrue((elementTop >= topPosition && elementTop < bottomPosition), "Item is not displayed on the screen.");
        softAssert.assertAll();

    }

}
