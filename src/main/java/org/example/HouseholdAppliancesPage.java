package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class HouseholdAppliancesPage extends HeaderPage {

    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Кліматична техніка']")
    WebElement climateControlEquipment;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Техніка для дому']")
    WebElement homeAppliances;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Світ кави']")
    WebElement coffeeWorld;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Техніка для кухні']")
    WebElement kitchenAppliances;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Персональний догляд']")
    WebElement personalisedCare;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Великопобутова техніка']")
    WebElement largeHouseholdAppliances;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Вбудована техніка']")
    WebElement built_inAppliances;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Аксесуари']")
    WebElement accessories;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Фільтри для води']")
    WebElement waterFilters;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Запчастини для побутової техніки']")
    WebElement sparePartsForHouseholdAppliances;

    public HouseholdAppliancesPage() {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10),this);

    }

}
