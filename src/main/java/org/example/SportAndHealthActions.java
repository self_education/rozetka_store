package org.example;

public class SportAndHealthActions extends HeaderActions {

    SportAndHealthPage sportAndHealthPage = new SportAndHealthPage();

    public SportAndHealthActions() {
        UrlVerifier.UrlVerifier("https://allo.ua/ua/sport-i-zdorov-e/");
    }

    public SportAndHealthActions clickBicyclesAndAccessories() {
        sportAndHealthPage.bicyclesAndAccessories.click();
        return this;
    }

    public SportAndHealthActions clickSkateboards() {
        sportAndHealthPage.skateboards.click();
        return this;
    }

    public SportAndHealthActions clickDecksForSkateboards() {
        sportAndHealthPage.decksForSkateboards.click();
        return this;
    }

    public SportAndHealthActions clickWheelsForSkateboards() {
        sportAndHealthPage.wheelsForSkateboards.click();
        return this;
    }

    public SportAndHealthActions clickAccessoriesForSkateboards() {
        sportAndHealthPage.accessoriesForSkateboards.click();
        return this;
    }

    public SportAndHealthActions clickSlimmingClothes() {
        sportAndHealthPage.slimmingClothes.click();
        return this;
    }

    public SportAndHealthActions clickSimulators() {
        sportAndHealthPage.simulators.click();
        return this;
    }

    public SportAndHealthActions clickSportsEquipment() {
        sportAndHealthPage.sportsEquipment.click();
        return this;
    }

    public SportAndHealthActions clickMartialArts() {
        sportAndHealthPage.martialArts.click();
        return this;
    }

    public SportAndHealthActions clickDarts() {
        sportAndHealthPage.darts.click();
        return this;
    }

    public SportAndHealthActions clickTennisAndSquash() {
        sportAndHealthPage.tennisAndSquash.click();
        return this;
    }

    public SportAndHealthActions clickRollersAndProtection() {
        sportAndHealthPage.rollersAndProtection.click();
        return this;
    }

    public SportAndHealthActions clickWinterSports() {
        sportAndHealthPage.winterSports.click();
        return this;
    }

    public SportAndHealthActions clickWaterSports() {
        sportAndHealthPage.waterSports.click();
        return this;
    }

    public SportAndHealthActions clickHealthAndCare() {
        sportAndHealthPage.healthAndCare.click();
        return this;
    }

    public SportAndHealthActions clickHearingAids() {
        sportAndHealthPage.hearingAids.click();
        return this;
    }

    public SportAndHealthActions clickCardiacSensors() {
        sportAndHealthPage.cardiacSensors.click();
        return this;
    }

    public SportAndHealthActions clickWalkieTalkies() {
        sportAndHealthPage.walkieTalkies.click();
        return this;
    }

    public SportAndHealthActions clickAccessoriesForWalkieTalkies() {
        sportAndHealthPage.accessoriesForWalkieTalkies.click();
        return this;
    }

    public SportAndHealthActions clickBasketball() {
        sportAndHealthPage.basketball.click();
        return this;
    }

    public SportAndHealthActions clickBilliards() {
        sportAndHealthPage.billiards.click();
        return this;
    }

    public SportAndHealthActions clickMiniGolf() {
        sportAndHealthPage.miniGolf.click();
        return this;
    }

    public SportAndHealthActions clickProtectiveEquipment() {
        sportAndHealthPage.protectiveEquipment.click();
        return this;
    }

    public SportAndHealthActions clickTrampolinesAndAccessories() {
        sportAndHealthPage.trampolinesAndAccessories.click();
        return this;
    }

    public SportAndHealthActions clickGymnastics() {
        sportAndHealthPage.gymnastics.click();
        return this;
    }

    public SportAndHealthActions clickHockey() {
        sportAndHealthPage.hockey.click();
        return this;
    }

    public SportAndHealthActions clickFigureSkating() {
        sportAndHealthPage.figureSkating.click();
        return this;
    }

    public SportAndHealthActions clickSportsCups() {
        sportAndHealthPage.sportsCups.click();
        return this;
    }

    public SportAndHealthActions clickSportsMedals() {
        sportAndHealthPage.sportsMedals.click();
        return this;
    }

    @Override
    public SportAndHealthActions verifyHeaderUrl(String url) {
        UrlVerifier.UrlVerifier(url);
        return this;
    }

}
