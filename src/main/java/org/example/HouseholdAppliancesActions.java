package org.example;

public class HouseholdAppliancesActions extends HeaderActions {
    HouseholdAppliancesPage householdAppliancesPage = new HouseholdAppliancesPage();

    public HouseholdAppliancesActions() {
        UrlVerifier.UrlVerifier("https://allo.ua/ua/bytovaya-tehnika/");
    }

    public HouseholdAppliancesActions clickClimateControlEquipment() {
        householdAppliancesPage.climateControlEquipment.click();
        return this;
    }
    
    public HouseholdAppliancesActions clickHomeAppliances(){
        householdAppliancesPage.homeAppliances.click();
        return this;
    }
    
    public HouseholdAppliancesActions clickCoffeeWorld(){
        householdAppliancesPage.coffeeWorld.click();
        return this;
    }
    
    public HouseholdAppliancesActions clickKitchenAppliances(){
        householdAppliancesPage.kitchenAppliances.click();
        return this;
    }
    
    public HouseholdAppliancesActions clickPersonalisedCare(){
        householdAppliancesPage.personalisedCare.click();
        return this;
    }
    
    public HouseholdAppliancesActions clickLargeHouseholdAppliances(){
        householdAppliancesPage.largeHouseholdAppliances.click();
        return this;
    }
    
    public HouseholdAppliancesActions clickBuilt_inAppliances(){
        householdAppliancesPage.built_inAppliances.click();
        return this;
    }
    
    public HouseholdAppliancesActions clickAccessories(){
        householdAppliancesPage.accessories.click();
        return this;
    }
    
    public HouseholdAppliancesActions clickWaterFilters(){
        householdAppliancesPage.waterFilters.click();
        return this;
    }
    
    public HouseholdAppliancesActions clickSparePartsForHouseholdAppliances(){
        householdAppliancesPage.sparePartsForHouseholdAppliances.click();
        return this;
    }

    @Override
    public HouseholdAppliancesActions verifyHeaderUrl(String url) {
        UrlVerifier.UrlVerifier(url);
        return this;
    }
}
