package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;


public class XiaomiPage extends HeaderPage {
    @FindBy(xpath = "//button[@class='slick-next slick-arrow']")
    WebElement nextButton;
    @FindBy(xpath = "//button[@class='slick-prev slick-arrow']")
    WebElement beforeButton;

    public XiaomiPage() {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }
}
