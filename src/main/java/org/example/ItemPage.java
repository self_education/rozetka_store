package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class ItemPage extends HeaderPage{

    @FindBy(xpath = "//h1")
    public WebElement title;
    @FindBy(xpath = "//td[@class = 'p-specs__cell' and normalize-space() = 'Тип телевізора']/following-sibling::td")
    public WebElement tvType;
    @FindBy(xpath = "//span[@class = 'p-tabs__sku-value']")
    public WebElement itemCode;
    @FindBy(xpath = "//p[@class='p-trade__stock-label']")
    public WebElement itemStockLabel;
    ItemPage(){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10),this);
    }

}
