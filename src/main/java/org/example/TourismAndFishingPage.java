package org.example;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class TourismAndFishingPage extends HeaderPage {

    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Намети']")
    WebElement tents;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Ліхтарі']")
    WebElement lanterns;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Термопродукція']")
    WebElement thermalProducts;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Рюкзаки']")
    WebElement backpacks;
    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Надувні човни']")
    WebElement inflatableBoats;

    @FindBy(xpath = "//a[@class = 'portal-category__item-link' and normalize-space() = 'Мотори для човнів']")
    WebElement motorsForBoats;


    public TourismAndFishingPage() {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

}
