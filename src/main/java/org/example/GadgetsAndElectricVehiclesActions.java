package org.example;

public class GadgetsAndElectricVehiclesActions extends HeaderActions {

    GadgetsAndElectricVehiclesPage gadgetsAndElectricVehiclesPage = new GadgetsAndElectricVehiclesPage();

    public GadgetsAndElectricVehiclesActions() {
        UrlVerifier.UrlVerifier("https://allo.ua/ua/gadzhety/");
    }

    public GadgetsAndElectricVehiclesActions click3D_Print() {
        gadgetsAndElectricVehiclesPage.print_3D.click();
        return this;
    }

    public GadgetsAndElectricVehiclesActions clickFitnessBracelets() {
        gadgetsAndElectricVehiclesPage.fitnessBracelets.click();
        return this;
    }

    public GadgetsAndElectricVehiclesActions clickAccessoriesForSmartwatches() {
        gadgetsAndElectricVehiclesPage.accessoriesForSmartwatches.click();
        return this;
    }

    public GadgetsAndElectricVehiclesActions clickElectricTransport() {
        gadgetsAndElectricVehiclesPage.electricTransport.click();
        return this;
    }

    public GadgetsAndElectricVehiclesActions clickGameZone() {
        gadgetsAndElectricVehiclesPage.gameZone.click();
        return this;
    }

    public GadgetsAndElectricVehiclesActions clickActionCamerasAndAccessories() {
        gadgetsAndElectricVehiclesPage.actionCamerasAndAccessories.click();
        return this;
    }

    public GadgetsAndElectricVehiclesActions clickTobaccoHeatingSystems() {
        gadgetsAndElectricVehiclesPage.tobaccoHeatingSystems.click();
        return this;
    }

    public GadgetsAndElectricVehiclesActions clickSmartKeyrings() {
        gadgetsAndElectricVehiclesPage.smartKeyrings.click();
        return this;
    }

    public GadgetsAndElectricVehiclesActions clickSmartRings() {
        gadgetsAndElectricVehiclesPage.smartRings.click();
        return this;
    }

    @Override
    public GadgetsAndElectricVehiclesActions verifyHeaderUrl(String url) {
        UrlVerifier.UrlVerifier(url);
        return this;
    }

}
