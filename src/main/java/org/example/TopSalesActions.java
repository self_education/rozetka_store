package org.example;

public class TopSalesActions extends HeaderActions{

    TopSalesPage topSalesPage = new TopSalesPage();

    public TopSalesActions() {
        UrlVerifier.UrlVerifier("https://allo.ua/ua/events-and-discounts/trymayte-zaryad-action/");
    }

    public TopSalesActions clickPromoDetails() {
        topSalesPage.promoDetails.click();
        return this;
    }

}
